<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $fillable = [
    	'program_id',
    	'user_id',
    	'stripe_response',
    	'amount',
    ];

    public function user(){
        return $this->hasOne(User::class ,'id','user_id');
    }
}
