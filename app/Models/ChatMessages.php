<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessages extends Model
{
    //
    protected $fillable = [
    	'to_user_id',
    	'from_user_id',
    	'messages',
    	'send_from',
    	'send_to',
    	'file',
    ];
}
