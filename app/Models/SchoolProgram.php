<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolProgram extends Model
{
    protected $fillable = [
        'university','country','tuition_fee','application_fee','program_level','program_length','cost_of_living'
        ,'university_logo_link_src','description','program'
    ];
}
