<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolsPartner extends Model
{
    //
     const STATUS_APPROVES = 1;
     const STATUS_REJECT = 2;

    protected $table = 'schools_partners';

    protected $fillable = [
    	'school_name',
    	'contact_first_name',
    	'contact_last_name',
    	'contact_email',
    	'phone_number',
    	'contact_title',
    	'class_partners',
    	'additional_partners',
    ];
}
