<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    protected $fillable = [
      'name_of_organization','from','to','designation','salary'
    ];
}
