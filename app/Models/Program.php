<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = [
      'tuition_fee','application_fee','program_level','program_length','program_name','school_id','description','program_type'
    ];

    public function school(){
        return $this->hasOne(School::class,'id','school_id');
    }

    public function schools(){
        return $this->hasMany(School::class,'id','school_id');
    }
}
