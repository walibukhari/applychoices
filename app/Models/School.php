<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
      'name','country','state','place','logo_link','logo'
    ];

    public function program(){
        return $this->hasMany(Program::class,'school_id','id');
    }
}
