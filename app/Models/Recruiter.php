<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recruiter extends Model
{
    //

     const STATUS_APPROVES = 1;
     const STATUS_REJECT = 2;
     
    protected $fillable = [
    	'user_name',
    	'phone_number',
    	'email_address',
    	'city',
    	'country',
    	'address',
    ];
}
