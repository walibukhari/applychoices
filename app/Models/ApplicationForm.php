<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ApplicationForm extends Model
{
    protected $fillable = [
      'user_id','name','email','dob','address','phone','last_edu','year_complete','ielts','future_plan','what_results','employment_id','status'
    ];
     public function user()
     {
     	return $this->hasOne(User::class , 'id' , 'user_id' );
     }
}
