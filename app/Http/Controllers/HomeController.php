<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Program;
use App\Models\School;
use App\Models\SchoolProgram;
use App\Models\State;
use App\Models\SchoolsPartner;
use App\Models\ApplicationForm;
use App\Models\Employment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function registerAs(Request $request)
    {
        User::where('id','=',\Auth::user()->id)->update([
           'register_as' => $request->register_as
        ]);
        $user = \Auth::user();
        return redirect('/home');
    }

    public function homePage()
    {
        $user = \Auth::user();
        return view('frontend.index',[
            'user' => $user
        ]);
    }

    public function schoolData($id){
        $school = School::where('id','=',$id)->first();
        return collect([
           'status' => true,
           'data' => $school
        ]);
    }

    public function searchPage(Request $request)
    {
        $countries = Country::with('states')->get();
        $states = State::all();
        $School = School::orderBy('id','desc')->get();
        $programLevel = Program::select('id','program_level','program_type')->get();
        return view('frontend.search.index',[
            'countries' => $countries,
            'states' => $states,
            'school' => $School,
            'program_level' => $programLevel,
            'filter' => isset($request->filter) ? $request->filter : '',
        ]);
    }

    public function userProfile(){
        // dd(123);
        return view('frontend.users.index');
    }
    public function updateProfile(Request $request){
        $user = User::where('id','=',$request->user_id)->first();
        $pass = bcrypt($request->password);
        $password = isset($pass) ? $pass : $user->password;
        User::where('id','=',$request->user_id)->update([
           'name' => $request->name,
           'email' => $request->email,
           'password' => $password,
        ]);

        return back()->with('success_me','Profile Update Successfully');
    }

    public function searchData(Request $request){

        if($request->header == 'true') {
            //search program
            $program = Program::with('schools')->OrderBy('id','desc');
            $School = School::with('program')->OrderBy('id','desc');
            if($request->tag1 != '') {
                $program = $program->where('program_name','LIKE','%'. $request->tag1.'%');
            }

            //serach school
            if($request->tag2 != '') {
                $School = $School->where('name','LIKE' ,'%' . $request->tag2 . '%')
                                 ->orwhere('country','LIKE' , '%' . $request->tag2 . '%');
            }

            if($request->tag1 != '' && $request->tag2 != '') {
                $program = $program->where('program_name','LIKE','%'. $request->tag1.'%');
                $School = $School->where('name','LIKE' , '%' . $request->tag2 . '%')
                    ->orwhere('country','LIKE' , '%' . $request->tag2 . '%');
            }
            if($request->tag1 != '' && $request->tag2 == '') {
                return collect([
                    'status' => true,
                    'program' => $program->get(),
                    'school' => []
                ]);
            }
            if($request->tag1 == '' && $request->tag2 != '') {
                return collect([
                    'status' => true,
                    'program' => [],
                    'school' => $School->get()
                ]);
            }
            if($request->tag1 == '' && $request->tag2 == '') {
                return collect([
                    'status' => true,
                    'program' => $program->get(),
                    'school' => $School->get()
                ]);
            }
            if(count($School->get()) > 0 && count($program->get()) > 0) {
                return collect([
                    'status' => true,
                    'program' => $program->get(),
                    'school' => $School->get()
                ]);
            }
        }


        if($request->filter == 'true') {
            //search filters

            //eligibility filters

            $School = School::orderBy('id', 'desc');
            if ($request->nationality != '') {
                $School = $School;
            }
            if (!is_null($request->country)) {
                $country = Country::where('id', '=', $request->country)->first();
                if (!is_null($country)) {
                    $School = $School->where('country', '=', $country->country);
                }
            }
            if ($request->program_length != '') {
                $programLength = $request->program_length;
                $School = $School->with(['program' => function ($q) use ($programLength) {
                    $q->where('program_name', 'LIKE', '%' . $programLength . '%');
                }]);
            }
            //school filters
            if ($request->countries != '') {
                $explodeC = explode(',', $request->countries);
                $cArray = [];
                if (isset($explodeC) && count($explodeC) > 1) {
                    foreach ($explodeC as $country) {
                        $c = Country::where('id', '=', $country)->first();
                        $cleanName = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $c->country)));
                        $cArray[] = $cleanName;
                    }
                } else{
                    $c = Country::where('id', '=', $explodeC[0])->first();
                    $cleanName = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $c->country)));
                    $cArray[] = $cleanName;
                }
                $School = School::whereIn('country', $cArray);
            }
            if ($request->state != '') {
                $explodeS = explode(',', $request->state);
                $cArray = [];
                if (isset($explodeS) && count($explodeS) > 1) {
                    foreach ($explodeS as $s) {
                        $c = State::where('id', '=', $s)->first();
                        $cArray[] = $c->state;
                    }
                } else {
                    $c = State::where('id', '=', $explodeS[0])->first();
                    $cArray[] = $c->state;
                }
                $School = School::whereIn('state', $cArray);
            }
            if ($request->university == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'university' . '%');
            }
            if ($request->college == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'college' . '%');
            }
            if ($request->high_school == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'school' . '%');
            }
            if ($request->schools != '') {
                $explodeC = explode(',', $request->schools);
                $cArray = [];
                foreach ($explodeC as $d) {
                    $cArray[] = (int)$d;
                }
                $School = School::whereIn('id', $cArray);
            }
            if ($request->schools != '' && $request->university == 'true') {
                $explodeC = explode(',', $request->schools);
                $cArray = [];
                foreach ($explodeC as $d) {
                    $cArray[] = (int)$d;
                }
                $School = School::whereIn('id', $cArray)
                    ->orwhere('name', 'LIKE', '%' . 'university' . '%');
            }
            if ($request->schools != '' && $request->high_school == 'true') {
                $explodeC = explode(',', $request->schools);
                $cArray = [];
                foreach ($explodeC as $d) {
                    $cArray[] = (int)$d;
                }
                $School = School::whereIn('id', $cArray)
                    ->orwhere('name', 'LIKE', '%' . 'school' . '%');
            }
            if ($request->high_school == 'true' && $request->university == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'school' . '%')
                    ->orwhere('name', 'LIKE', '%' . 'university' . '%');
            }
            if ($request->college == 'true' && $request->university == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'college' . '%')->orwhere('name', 'LIKE', '%' . 'university' . '%');
            }
            if ($request->college == 'true' && $request->university == 'true' && $request->high_school == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'college' . '%')
                    ->orwhere('name', 'LIKE', '%' . 'university' . '%')
                    ->orwhere('name', 'LIKE', '%' . 'school' . '%');
            } else if ($request->high_school == 'true' && $request->college == 'true') {
                $School = School::where('name', 'LIKE', '%' . 'college' . '%')
                    ->orwhere('name', 'LIKE', '%' . 'school' . '%');
            }
            if ($request->schools != '' && $request->college == 'true') {
                $explodeC = explode(',', $request->schools);
                $cArray = [];
                foreach ($explodeC as $d) {
                    $cArray[] = (int)$d;
                }
                $School = School::whereIn('id', $cArray)
                    ->orwhere('name', 'LIKE', '%' . 'college' . '%');
            }
            if ($request->eng_institute != '') {
                $School = School::orderBy('id', 'desc');
            }

            return collect([
                'status' => true,
                'school' => $School->with('program')->get(),
            ]);
        }

        //program filters
        if($request->program_filters == 'true') {
            $Program = Program::with('schools')->OrderBy('id','desc');
            if ($request->range1 != '') {
                $Program = $Program->where('tuition_fee','LIKE','%'.$request->range1.'%');
            }
            if ($request->range2 != '') {
                $Program = $Program->where('application_fee','LIKE','%'.$request->range2.'%');
            }
            if ($request->program_level != '') {
                $explodeP = explode(',', $request->program_level);
                $cArray = [];
                foreach ($explodeP as $d) {
                    $cArray[] = (int)$d;
                }
                $Program = $Program->whereIn('id',$cArray);
            }
            if ($request->range1 != '' && $request->range2 != '' && $request->program_level != '') {
                $explodeP = explode(',', $request->program_level);
                $cArray = [];
                foreach ($explodeP as $d) {
                    $cArray[] = (int)$d;
                }
                $Program = $Program->where('tuition_fee','LIKE','%'.$request->range1.'%')
                    ->orwhere('application_fee','LIKE','%'.$request->range2.'%')
                    ->orwhereIn('id',$cArray);
            } else if ($request->range1 != '' && $request->range2 != '') {
                $Program = $Program->where('tuition_fee','LIKE','%'.$request->range1.'%')
                ->orwhere('application_fee','LIKE','%'.$request->range2.'%');
            } else if ($request->range1 != '' && $request->program_level != '') {
                $explodeP = explode(',', $request->program_level);
                $cArray = [];
                foreach ($explodeP as $d) {
                    $cArray[] = (int)$d;
                }
                $Program = $Program->where('tuition_fee','LIKE','%'.$request->range1.'%')
                    ->orwhere('id',$cArray);
            } else if ($request->range2 != '' && $request->program_level != '') {
                $explodeP = explode(',', $request->program_level);
                $cArray = [];
                foreach ($explodeP as $d) {
                    $cArray[] = (int)$d;
                }
                $Program = $Program->where('tuition_fee','LIKE','%'.$request->range2.'%')
                    ->orwhereIn('id',$explodeP);
            }

            if($request->program_type != '') {
                $Program = $Program->where('program_type','=',$request->program_type);
            }
            return collect([
                'status' => true,
                'program' => $Program->get()
            ]);
        }
    }
    public function studentPage()
    {
        return view('frontend.student.student');
    }
    public function schoolPartner()
    {
        return view('frontend.school_partners.index');
    }
    public function agentPage()
    {
        return view('frontend.recruiter.recruiter');
    }

    public function getAutoCompleteData(Request  $request){
        $keyWord = $request->term;
        $data = Program::where('program_name', 'LIKE' , '%' . $keyWord . '%')
            ->select('program_name')->get();
        return response()->json($data);
    }

    public function locationData(Request  $request){
        $keyWord = $request->q;
        $data = School::where('name','LIKE','%'.$keyWord.'%')
            ->orwhere('country', 'LIKE','%' . $keyWord . '%')
            ->select('name','country')
            ->get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $school = $request->all();

        $this->validate($request,[
            'school_name'=> 'required',
            'contact_first_name'=> 'required',
            'contact_last_name'=> 'required',
            'contact_email'=>'required',
            'class_partners'=> 'required',
            'additional_partners' => 'required',
            'phone_number'=> 'required',
            'contact_title'=> 'required',
        ]);
        SchoolsPartner::create([
            'school_name'=>$request['school_name'],
            'contact_last_name'=>$request['contact_last_name'],
            'contact_first_name'=>$request['contact_first_name'],
            'class_partners'=>$request['class_partners'],
            'additional_partners'=>$request['additional_partners'],
            'contact_email'=>$request['contact_email'],
            'contact_title'=>$request['contact_title'],
            'phone_number'=>$request['phone_number'],
        ]);
        return back()->with('success_msg','DATA ADDED SUCCESSFULLY......!');

    }
    public function agentDashboard()
    {
        return view('frontend.recruiter.index');
    }
    public function appForm($id)
    {
        // dd($request->all());
        $program = Program::where('id','=',$id)->with('school')->first();

        return view('frontend.applicationForm.index',[
            'program' => $program
        ]);
    }
    public function recruiterLogout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/home');
    }
    public function userLogout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/home');
    }
    public function storeApplication(Request $request)
    {
        $request = $request->all();
        $arr = [];
        foreach ($request['name_of_organization'] as $key => $value) {
            $employement = Employment::create([
                'name_of_organization' => $value,
            ]);
            array_push($arr,$employement->id);
        }
        foreach ($request['form'] as $key => $value) {
            Employment::where('id','=',$arr[$key])->update([
                'from' => $value,
            ]);
        }
        foreach ($request['to'] as $key => $value) {
            Employment::where('id','=',$arr[$key])->update([
                'to' => $value,
            ]);
        }
        foreach ($request['designation'] as $key => $value) {
            Employment::where('id','=',$arr[$key])->update([
                'designation' => $value,
            ]);
        }
        foreach ($request['salary'] as $key => $value) {
            Employment::where('id','=',$arr[$key])->update([
                'salary' => $value,
            ]);
        }
        ApplicationForm::create([
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'dob' => $request['dob'],
            'email' => $request['email'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'last_edu' => $request['last_edu'],
            'year_complete' => $request['year_complete'],
            'ielts' => $request['ielts'],
            'future_plan' => $request['future_plan'],
            'what_results' => $request['what_results'],
            'employment_id' => json_encode($arr)
        ]);
         return back()->with('success_msg','DATA SUBMITED SUCCESSFULLY......!');
    }
}
