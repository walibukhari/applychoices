<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Stripe;
use Illuminate\Support\Facades\Session;

class StripeController extends Controller
{
    /**
     * payment view
     */
    public function handleGet(request $request)
    {
       // dd($request->amount);
        $data['payment'] = $request->final_payment;
        $data['customer_id'] = $request->customer_id;
        $data['discount_code'] = $request->discount_code;
        $data['paymentmethod_id'] = $request->paymentmethod_id;
        $data['prooduct_id']  = $request->id_product;
        $data['amount']  = $request->amount;
         return view('front.stripe_payment',$data);
    }

    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
         // dd($request->id_product);
        Stripe\Stripe::setApiKey('sk_test_51HTlCJErqGk6bYCnehW5s1Pr3HLDuRhn2szWWg5K8QuxmrjYfcvPEGS6b9ARPnmnt5OC0F7xK3nWCIOqNWZb5jb300VZE7LYnd');
        $stripe = Stripe\Charge::create ([
                "amount" => 20*100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "ApplyChoices customer purchasing" ,
        ]);
        Transaction::create([
            'program_id' => $request->program_id,
            'amount' => 2000,
            'user_id' => $request->user_id,
            'stripe_response' => json_encode($stripe),
        ]);
        $data = ['status' => 'okay'];
        return collect([
           'status' => true,
           'data' => $data
        ]);
    }
}
