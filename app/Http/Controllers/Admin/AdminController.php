<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ApplicationForm;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function login(){
        return view('backend.login');
    }
    public function postLogin(Request $request){
        if(!Auth::guard('admin')->attempt(['email' => $request->email,'password' => $request->password])) {
            return back()->with('error_ad','please enter valid credentials');
        }
        return redirect('/admin/home');
    }
    public function index(){
        return view('backend.index');
    }
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }

    public function getTransaction(){
        $transaction = Transaction::with('user')->get();
        return view('backend.transaction',[
            'transaction' => $transaction
        ]);
    }
    public function getApplications(){
        $app = ApplicationForm::with('user.transaction')->whereHas('user.transaction')->get();
        return view('backend.applications',[
            'app' => $app
        ]);
    }
}
