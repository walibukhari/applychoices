<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\ChatMessages;

class ChatController extends Controller
{
    //
    public function index()
    {
    	// dd(123);
    	return view('backend.chat.index');
    }
    public function get_all_recruiters()
    {
    	$user = User::where('register_as','=',User::AS_RECRUITER)->get();
    	// dd($user);
    	return collect([
    		'status' => true,
    		'data' => $user
    	]);
    }
    public function postMessage(Request $request)
    {
    	$adminId = \Auth::guard('admin')->user()->id;
    	if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = 'uploads/chat';
            $fileName = $path.'/'.$name;
            $request->file('file')->move($path,$name);
            ChatMessages::create([
                'to_user_id' => $request->recruiter_id,
                'from_user_id' => $adminId,
                'send_from' => 'Admin',
                'send_to' => $request->recruiter_name,
                'file' => $fileName,
            ]);
        } else {
            ChatMessages::create([
                'to_user_id' => $request->recruiter_id,
                'from_user_id' => $adminId,
                'send_from' => 'Admin',
                'send_to' => $request->recruiter_name,
                'messages' => $request->message,
            ]);
        }
    	return collect([
    		'status' => true,
    		'message' => 'message send successfully',
    	]);
    }
    public function getMessage($id)
    {
    	// dd($id);
    	$messages = ChatMessages::where('to_user_id','=',$id)->orwhere('from_user_id','=',$id)->get();
    	// dd($messages);
    	return collect([

    		'status' => true,
    		'data' => $messages,
    	]);

    }
    public function adminChat()
    {
        // dd(123456);
         return view('backend.userchat.index');

    }
    public function getStudents()
    {
        // dd(123);
        $student = User::where('register_as','=',User::AS_STUDENT)->get();
        // dd($user);
        return collect([
            'status' => true,
            'data' => $student
        ]);
    }
}
