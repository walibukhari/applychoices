<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\School;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function addProgram(){
        $school = School::all();
        return view('backend.program.index',[
            'school' => $school
        ]);
    }
    public function allProgram(){
        $program = Program::with('school')->get();
        return view('backend.program.all',[
            'program' => $program
        ]);
    }
    public function saveProgram(Request  $request){
       Program::create([
            'tuition_fee' =>  number_format($request->tuition_fee,2),
            'program_level' => $request->program_level,
            'program_length' => $request->program_length,
            'program_name' => $request->program_name,
            'school_id' => $request->school_id,
           'description' => $request->description,
           'application_fee' => number_format($request->application_fee,2),
           'program_type' => $request->program_type
        ]);
        return back()->with('success_message','program added successfully');
    }
    public function deleteProgram($id){
        Program::where('id','=',$id)->delete();
        return redirect()->route('all.program')->with('program_message','program deleted successfully');
    }
    public function editProgram($id){
        $school = School::all();
        $program = Program::where('id','=',$id)->first();
        return view('backend.program.edit',[
            'program' => $program,
            'school' => $school,
        ]);
    }
    public function updateProgram(Request $request){
        Program::where('id','=',$request->id)->update([
            'tuition_fee' => number_format($request->tuition_fee,2),
            'program_level' => $request->program_level,
            'program_length' => $request->program_length,
            'program_name' => $request->program_name,
            'school_id' => $request->school_id,
            'description' => $request->description,
            'application_fee' => number_format($request->application_fee , 2),
            'program_type' => $request->program_type
        ]);
        return redirect()->route('all.program')->with('program_message','program update successfully');
    }
}
