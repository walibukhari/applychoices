<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SchoolsPartner;

class PartnerController extends Controller
{
    //
    public function index()
    {
    	// dd(123);
    	$partners = SchoolsPartner::all();

    	// dd($partners);

    	return view('backend.partners.index',['partners' => $partners]);
    }
    public function approve($id)
    {
    	// dd(123);
    	   SchoolsPartner::where('id','=',$id)->update([
    		'status' => SchoolsPartner::STATUS_APPROVES
    	]);
    	return back()->with('success_msg','UPDATED SUCCESSFLLY...!');
    }
    public function reject($id)
    {
    	SchoolsPartner::where('id','=',$id)->update([

    		'status' => SchoolsPartner::STATUS_REJECT
    	]);
    	return back()->with('success_msg','REJECTED SUCCESSFLLY...!');
    }
}
