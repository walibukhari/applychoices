<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function addSchool(){
        return view('backend.school.index');
    }
    public function allSchool(){
        // dd(123);
        $data = School::all();
        return view('backend.school.all',[
            'data' => $data
        ]);
    }
    public function saveSchool(Request $request){
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = 'uploads/school/logo';
            $fileName = $path.'/'.$name;
            $file->move($path,$fileName);
        }
        School::create([
            'name' => $request->name,
            'country' => $request->country,
            'state' => $request->state,
            'place' => $request->place,
            'logo_link' => $request->logo_link,
            'logo' => isset($fileName) ? $fileName : ''
        ]);

        return back()->with('success_message','school added successfully');
    }

    public function editSchool($id) {
        $school = School::where('id','=',$id)->first();
        return view('backend.school.edit',[
            'school' => $school
        ]);
    }
    public function deleteSchool($id) {
        School::where('id','=',$id)->delete();
        return redirect()->route('all.school')->with('school_message','school deleted successfully');
    }
    public function updateSchool(Request  $request) {
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = 'uploads/school/logo';
            $fileName = $path.'/'.$name;
            $file->move($path,$fileName);
        }
        School::where('id','=',$request->id)->update([
            'name' => $request->name,
            'country' => $request->country,
            'state' => $request->state,
            'place' => $request->place,
            'logo_link' => $request->logo_link,
            'logo' => isset($fileName) ? $fileName : ''
        ]);
        return redirect()->route('all.school')->with('school_message','school updated successfully');
    }
}
