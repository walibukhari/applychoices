<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Exports\UsersExport;
use App\Exports\ProgramsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Program;

class ExcelController extends Controller
{
    //
  public function index() 
    {
    	// dd(123);
        return Excel::download(new UsersExport, 'users.xlsx');
    }

  public function program()
    {
    	// dd(123);
    	return Excel::download(new ProgramsExport, 'programs.xlsx');
    }
}
