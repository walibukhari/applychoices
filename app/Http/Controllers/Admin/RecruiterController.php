<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recruiter;

class RecruiterController extends Controller
{
    //
    public function index()
    {
    	// dd(1234);
    	$recruiters = Recruiter::all();

    	return view('backend.recruiters.index',['recruiters'=> $recruiters]);
    }
    public function approve($id)
    {
    	// dd(123);
    	   Recruiter::where('id','=',$id)->update([
    		'status_recruiter' => Recruiter::STATUS_APPROVES
    	]);
    	return back()->with('success_msg','UPDATED SUCCESSFLLY...!');
    }
    public function reject($id)
    {
    	Recruiter::where('id','=',$id)->update([

    		'status_recruiter' => Recruiter::STATUS_REJECT
    	]);
    	return back()->with('success_msg','REJECTED SUCCESSFLLY...!');
    }
}
