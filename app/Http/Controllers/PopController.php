<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Session;

class PopController extends Controller
{
    //
    public function index()
    {
    	// dd(123);
    	return view('payment.index');
    }
    public function store(Request $request)
    {
    	// dd($request->all());
    	$transaction = $request->all();

    	Transaction::create([
    		'application_form_id' => $request->name_of_card,
    		'amount' => $request->card_number,
    		'user_id' => $request->cvs_number,
    		'stripe_response' => $request->expiration_month,
    	]);
    	Session::flash('success_msg','TRANSACTION SUBMITED SUCCESSFULLY....!');

    	return back();
    }
}
