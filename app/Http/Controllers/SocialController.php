<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class SocialController extends Controller
{
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function redirectFB(){
        return Socialite::driver('facebook')->redirect();
    }

    public function callback($provider){
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user,true);
        return redirect('/home');
    }

    public function callbackFB(){
        $getInfo = Socialite::driver('facebook')->user();
        $user = $this->createUser($getInfo,'facebook');
        auth()->login($user,true);
        return redirect('/home');
    }

    public function createUser($userInfo , $provider){
        try {
            $user = User::where('provider_id', '=', $userInfo->id)->first();
            if (is_null($user)) {
                $user = User::create([
                    'name' => $userInfo->name,
                    'email' => $userInfo->email,
                    'provider' => $provider,
                    'provider_id' => $userInfo->id,
                    'register_as' => User::AS_RECRUITER
                ]);
                return $user;
            } else {
                return $user;
            }
        } catch (\Exception $e) {
            //Authentication failed
            return redirect()
                ->back()
                ->with('status','authentication failed, please try again!'.$e->getMessage());
        }
    }

//    public function sendMail($user){
//        $senderEmail = "chatads056@gmail.com";
//        $senderName  ='Classifieds';
//        $receiverEmail = $user->email;
//        Mail::send('mails.successRegister', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
//            $message->from($senderEmail, $senderName);
//            $message->to($receiverEmail)
//                ->subject('successfully registered');
//        });
//    }
}
