<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChatMessages;

class UserChatController extends Controller
{
    //
    public function index()
    {
    	// dd(123);
    	return view('frontend.recruiter.chat');
    }

    public function getMessages($id){
    	$messages = ChatMessages::where('to_user_id','=',$id)->orwhere('from_user_id','=',$id)->get();
    	return collect([
    		'status' => true,
    		'data' => $messages
    	]);
    }

    public function postMessage(Request $request)
    {
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = 'uploads/chat';
            $fileName = $path.'/'.$name;
            $request->file('file')->move($path,$name);
            ChatMessages::create([
                'to_user_id' => $request->admin_id,
                'from_user_id' => $request->recruiter_id,
                'send_from' => $request->recruiter_name,
                'send_to' => 'Admin',
                'file' => $fileName,
            ]);
        } else {
            ChatMessages::create([
                'to_user_id' => $request->admin_id,
                'from_user_id' => $request->recruiter_id,
                'send_from' => $request->recruiter_name,
                'send_to' => 'Admin',
                'messages' => $request->message,
            ]);
        }
    	return collect([
    		'status' => true,
    		'message' => 'message send successfully',
    	]);
    }
    public function getMessage($id,$name)
    {
    	// dd($id);

    	$messages = ChatMessages::where('from_user_id','=',$id)->where('send_from','=',$name)->get();
    	// dd($messages);
    	return collect([

    		'status' => true,
    		'data' => $messages,
    	]);

    }

    public function getDownload($id){
        $chat = ChatMessages::where('id','=',$id)->first();
        $file= $chat->file;
        $explode = explode('chat/',$file);
        $filename = $explode[1];
        // Define the path and the extension
        $file = public_path() . "/uploads/chat/" . $filename;
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if($ext == 'png' || 'PNG'){
            $headers = array(
                'Content-Type:image/png',
            );
        }
        else if($ext == 'jpg' || 'jpeg' || 'JPEG' || 'JPG'){
            $headers = array(
                'Content-Type:image/jpeg',
            );
        }
        else if($ext == 'gif' || 'GIF'){
            $headers = array(
                'Content-Type:image/gif',
            );
        } else if($ext == 'docx') {
            $headers = array(
                'Content-Type:application/docx',
            );
        } else if($ext == 'pdf') {
            $headers = array(
                'Content-Type:application/pdf',
            );
        }
        $response = response()->download($file, $filename, $headers);

        return $response;
    }

}
