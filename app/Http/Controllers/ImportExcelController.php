<?php

namespace App\Http\Controllers;

use App\Imports\ImportCountry;
use App\Imports\SchoolProgramImport;
use App\Imports\ImportState;
use App\Models\SchoolProgram;
use Illuminate\Http\Request;
use DB;
use Excel;

class ImportExcelController extends Controller
{
    function index()
    {
        $data = DB::table('school_programs')->get();
        return view('frontend.import_excel', compact('data'));
    }

    function import(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();
        $data = Excel::import(new SchoolProgramImport ,$request->file('select_file'));
        return back()->with('success', 'Excel Data Imported successfully.');
    }
}
