<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Recruiter;

class RecruiterController extends Controller
{
    //

public function agentDashboard(){
	// dd(123);

	$recruiters = Recruiter::all();
	return view('frontend.recruiter.index',['recruiters'=> $recruiters]);
}

public function postUploadImage(Request $request){
    // dd($request->file);
    if($request->hasFile('file')) {
        $file = $request->file('file');
        $name = $file->getClientOriginalName();
        $path = 'uploads/recruiter/';
        $fileName = $path.'/'.$name;
        $file->move($path,$name);
    }
    User::where('id','=',$request->id)->update([
        'image' => $fileName
    ]);

    return collect([
        'status' => true,
        'message' => 'image upload successfully'
    ]);
}

    public function index()
    {
    	return view('frontend.recruiter.index');
    }

    public function store(Request $request)
    {
    	// dd($request->all());
    	$recruiter = $request->all();
    	$this->validate($request,[
    		'user_name'=> 'required',
    		'phone_number'=> 'required',
    		'email_address'=> 'required',
    		'address'=> 'required',
    		'city'=> 'required',
    		'country'=> 'required',
    	]);
    	Recruiter::create([
    		'user_name'=> $request['user_name'],
    		'phone_number'=> $request['phone_number'],
    		'email_address'=> $request['address'],
    		'city'=> $request['city'],
    		'country'=> $request['country'],
    		'address'=> $request['address'],
    	]);
    	return redirect()->route('recruiter.submit');
    }
    public function phoneNumber(Request $request)
    {
        User::where('id','=',\Auth::user()->id)->update([
            'phone_number' => $request->phone_number
        ]);
        return back();
    }
    public function postName(Request $request)
    {
        User::where('id','=',\Auth::user()->id)->update([
            'name' => $request->name
        ]);
        return back();
    }
    public function chatName()
    {
        // dd(123);
        return view('frontend.userchat.index');
    }
}
