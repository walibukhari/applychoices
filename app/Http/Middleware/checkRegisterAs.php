<?php

namespace App\Http\Middleware;

use Closure;

class checkRegisterAs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Auth::check()) {
            return $next($request);
        } else {
            if(\Auth::user()->register_as == '') {
                return redirect('/home');
            } else {
                return $next($request);
            }
        }
    }
}
