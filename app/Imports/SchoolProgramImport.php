<?php

namespace App\Imports;

use App\Models\SchoolProgram;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SchoolProgramImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SchoolProgram([
           "university" => $row['university'],
           "country" => $row['country'],
           "tuition_fee" => $row['tuition_fee'],
           "application_fee" => $row['application_fee'],
           "program_length" => $row['program_length'],
           "cost_of_living" => $row['cost_of_living'],
           "university_logo_link_src" => $row['university_logo_link_src'],
           "description" => $row['description'],
           "program" => $row['program'],
        ]);
    }
}
