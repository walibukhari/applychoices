@extends('layouts.default')

@push('css')
    <style>
        .AccountForm input[type='email'] {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
        .AccountForm input[type='password'] {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
    </style>
@endpush

@section('content')
<section class="Section-AccountRegister">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="AccountHead">
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s">Login to Start
{{--                        <br /> Applying to Schools</h1>--}}
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-3 col-md-3 col-12"></div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                        <div class="AccountForm wow fadeInUp" data-wow-delay="0.2s">
                            <label>Email</label>
                            <input type="email" id="" required name="email" placeholder="enter email address">
                        </div>

                        <div class="AccountForm wow fadeInUp" data-wow-delay="0.4s">
                            <label>Password</label>
                            <input type="password" id="" required name="password" placeholder="enter password">
                        </div>

                        <div class="AccountForm wow fadeInUp" data-wow-delay="0.8s">
                            <button type="submit">Login</button>
                        </div>
                        </form>
                        <div class="clearfix"></div>

                        <div class="AlreadyExist">
                            <h3 class="wow fadeInUp" data-wow-delay="0.2s">Register account? <a href="{{route('register')}}">Register</a></h3>
{{--                            <h3 class="wow fadeInUp" data-wow-delay="0.4s">By joining ApplyBoard, you agree to ApplyChoices--}}
{{--                                <a href="#">Student</a> <a href="#">Terms & Conditions</a>--}}
{{--                                <a href="#">Privacy Policy</a> and <a href="#">Terms of Use</a>--}}
{{--                            </h3>--}}
                        </div>

                        <div class="LoginWith-Fb wow fadeInUp" data-wow-delay="0.2s">
                            <a href="{{url('/redirect/facebook')}}"><i class="fa fa-facebook-f"></i> Sign In With Facebook</a>
                        </div>

                        <div class="LoginWith-GooglePlus wow fadeInUp" data-wow-delay="0.4s">
                            <a href="{{url('/auth/redirect/google')}}"><i class="fa fa-google-plus"></i> Sign In With Google</a>
                        </div>

                    </div>

                    <div class="col-xl-3 col-md-3 col-12"></div>

                </div>
            </div>

        </div>
    </div>
</section>
@endsection

@push('js')
@endpush
