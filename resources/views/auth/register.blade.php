@extends('layouts.default')

@push('css')
    <style>
        .AccountForm select {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
        .AccountForm input[type='email'] {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
        .AccountForm input[type='password'] {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
    </style>
@endpush

@section('content')
    <section class="Section-AccountRegister">
        <div class="container">
            <div class="row">

                <div class="col-xl-12 col-md-12 col-12">
                    <div class="AccountHead">
                        <h1 class="wow fadeInUp" data-wow-delay="0.4s">Complete Your Registration to Start
                            <br /> Applying to Schools</h1>
                    </div>
                </div>

                <div class="col-xl-12 col-md-12 col-12">
                    <div class="row">

                        <div class="col-xl-3 col-md-3 col-12"></div>
                        <div class="col-xl-6 col-md-6 col-12">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="AccountForm wow fadeInUp" data-wow-delay="0.2s">
                                    <label>Name</label>
                                    <input type="text" required id="" name="name" placeholder="">
                                </div>
                            <div class="AccountForm wow fadeInUp" data-wow-delay="0.2s">
                                <label>Email</label>
                                <input type="email" required id="" name="email" placeholder="">
                            </div>

                            <div class="AccountForm wow fadeInUp" data-wow-delay="0.4s">
                                <label>Password</label>
                                <input type="password" required id="" name="password" placeholder="">
                            </div>

                            <div class="AccountForm wow fadeInUp" data-wow-delay="0.6s">
                                <label>Confrim Password</label>
                                <input type="password" id="" required name="password_confirmation" placeholder="">
                            </div>

                            <div class="AccountForm wow fadeInUp" data-wow-delay="0.6s">
                                <label>Register As</label>
                                <select name="register_as" required>
                                    <option value="1">student</option>
                                    <option value="2">school partner</option>
                                    <option value="3">recruiter</option>
                                </select>
                            </div>

                            <div class="AccountForm wow fadeInUp" data-wow-delay="0.8s">
                                <button type="submit">Register</button>
                            </div>
                        </form>

                            <div class="clearfix"></div>

                            <div class="AlreadyExist">
                                <h3 class="wow fadeInUp" data-wow-delay="0.2s">Already have an account? <a href="{{route('login')}}">Login</a></h3>
                                <h3 class="wow fadeInUp" data-wow-delay="0.4s">By joining ApplyBoard, you agree to ApplyChoices
                                    <a href="#">Student</a> <a href="#">Terms & Conditions</a>
                                    <a href="#">Privacy Policy</a> and <a href="#">Terms of Use</a>
                                </h3>
                            </div>

                            <div class="LoginWith-Fb wow fadeInUp" data-wow-delay="0.2s">
                                <a href="{{url('/redirect/facebook')}}"><i class="fa fa-facebook-f"></i> Sign Up With Facebook</a>
                            </div>

                            <div class="LoginWith-GooglePlus wow fadeInUp" data-wow-delay="0.4s">
                                <a href="{{url('/auth/redirect/google')}}"><i class="fa fa-google-plus"></i> Sign Up With Google</a>
                            </div>

                        </div>

                        <div class="col-xl-3 col-md-3 col-12"></div>

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@push('js')
@endpush
