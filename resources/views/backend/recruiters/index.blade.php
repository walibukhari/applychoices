@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Create School</li>
            <li class="breadcrumb-item active">
                <a href="javascript:;" class="btn btn-sm btn-outline-info" onclick="window.location.href='{{route('all.school')}}'">
                    All School
                </a>
            </li>
        </ol>
        <div class="row">
        	 <div class="col-md-12">
                @if(session()->has('success_msg'))
                    <div class="alert alert-success">
                        {{session()->get('success_msg')}}
                    </div>
                @endif

          <div class="table-responsive">
                   <table class="table table-striped">
                           <thead>
                                 <tr>
                                     <th>User Name</th>
                                     <th>Phone Number</th>
                                     <th>City</th>
                                     <th>Email Address</th>
                                     <th>Address</th>
                                     <th>Country</th>
                                     <th>Actions</th>
                                   </tr>
                             </thead>

                         <tbody>
                              @foreach($recruiters as $recruiter)
                                <tr>
                                  <td>{{$recruiter->user_name}}</td>
                                  <td>{{$recruiter->phone_number}}</td>
                                  <td>{{$recruiter->city}}</td>
                                  <td>{{$recruiter->email_address}}</td>
                                  <td>{{$recruiter->address}}</td>
                                  <td>{{$recruiter->country}}</td>
                                  <td>
                                	@if($recruiter->status_recruiter == 1)
                                	<a class="btn btn-outline-secondary btn-sm">Approved</a>
                                	@elseif($recruiter->status_recruiter == 2)
                                    <a href="" class="btn btn-outline-danger btn-sm">Rejected</a>
                               		@else
                               		<a href="{{route('recruiters.approve',[$recruiter->id])}}" class="btn btn-outline-secondary btn-sm">Approve</a>
                                    <a href="{{route('recruiters.reject',[$recruiter->id])}}" class="btn btn-outline-danger btn-sm">Reject</a>
                                	@endif
                                    <a href="{{ route('chat.views') }}" class="btn btn-outline-danger btn-sm">Chat</a>
                                </td>
                                </tr>
                               @endforeach
                        </tbody>
                   </table>
                </div>
              </div> 
        </div>
    </div>
@endsection


@push('js')

@endpush
