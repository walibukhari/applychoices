@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Create School</li>
            <li class="breadcrumb-item active">
                <a href="javascript:;" class="btn btn-sm btn-outline-info" onclick="window.location.href='{{route('all.school')}}'">
                    All School
                </a>
            </li>
        </ol>
        <div class="row">
        	 <div class="col-md-12">
                @if(session()->has('success_msg'))
                    <div class="alert alert-success">
                        {{session()->get('success_msg')}}
                    </div>
                @endif

          <div class="table-responsive">
                   <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>School Name</th>
                                <th>Contact First Name</th>
                                <th>Contact Last Name</th>
                                <th>Contact Email</th>
                                <th>Phone Number</th>
                                <th>Contact Title</th>
                                <th>Additional Partner</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                       <tbody>
                        @foreach($partners as $partner)
                            <tr>
                                <td>{{$partner->id}}</td>
                                <td>{{$partner->school_name}}</td>
                                <td>{{$partner->contact_first_name}}</td>
                                <td>{{$partner->contact_last_name}}</td>
                                <td>{{$partner->contact_email}}</td>
                                <td>{{$partner->phone_number}}</td>
                                <td>{{$partner->contact_title}}</td>
                                <td>{{$partner->additional_partners}}</td>
                                <td>
                                	@if($partner->status == 1)
                                	<a class="btn btn-outline-secondary btn-sm">Approved</a>
                                	@elseif($partner->status == 2)
                                    <a href="" class="btn btn-outline-danger btn-sm">Rejected</a>
                               		@else
                               		<a href="{{route('status.approve',[$partner->id])}}" class="btn btn-outline-secondary btn-sm">Approve</a>
                                    <a href="{{route('status.reject',[$partner->id])}}" class="btn btn-outline-danger btn-sm">Reject</a>
                                	@endif
                                    
                                </td>
                            </tr>
                        @endforeach
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')

@endpush
