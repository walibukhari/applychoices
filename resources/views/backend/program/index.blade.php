@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Create Program</li>
            <li class="breadcrumb-item active">
                <a href="javascript:;" class="btn btn-sm btn-outline-info" onclick="window.location.href='{{route('all.program')}}'">
                    All Program
                </a>
            </li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{session()->get('success_message')}}
                    </div>
                @endif
                <form action="{{route('store.program')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email">Select : School,University,College</label>
                        <select class="form-control" required name="school_id">
                            @foreach($school as $sc)
                                <option value="{{$sc->id}}">{{$sc->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Program:</label>
                        <input type="text" required name="program_name" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Description:</label>
                        <textarea cols="6" rows="6" class="form-control" required name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="email">Application Fee:</label>
                        <input type="text" name="application_fee" required class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Tuition Fee:</label>
                        <input type="text" required class="form-control" name="tuition_fee">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Program Level:</label>
                        <input type="text" class="form-control" name="program_level">
                    </div>
                    <div class="form-group">
                        <label for="pwd">program Length:</label>
                        <input type="text" required class="form-control" name="program_length">
                    </div>
                    <div class="form-group">
                        <label for="pwd">program type:</label>
                        <select class="form-control" name="program_type" required>
                            <option selected>Select Type</option>
                            <option value="post_graduate">Post Graduate</option>
                            <option value="under_graduate">Under Graduate</option>
                            <option value="high_schools">High Schools</option>
                        </select>
                    </div>
{{--                    <div class="form-group">--}}
{{--                        <label for="pwd">Program Level:</label>--}}
{{--                        <select class="form-control" name="program_level">--}}
{{--                            <option value="Grade 1">Grade 1</option>--}}
{{--                            <option value="Grade 2">Grade 2</option>--}}
{{--                            <option value="Grade 3">Grade 3</option>--}}
{{--                            <option value="Grade 4">Grade 4</option>--}}
{{--                            <option value="Grade 5">Grade 5</option>--}}
{{--                            <option value="Grade 6">Grade 6</option>--}}
{{--                            <option value="Grade 7">Grade 7</option>--}}
{{--                            <option value="Grade 8">Grade 8</option>--}}
{{--                            <option value="Grade 9">Grade 9</option>--}}
{{--                            <option value="Grade 10">Grade 10</option>--}}
{{--                            <option value="Grade 11">Grade 11</option>--}}
{{--                            <option value="Grade 12">Grade 12</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                    {{-- post gradudate --}}
{{--                    <div class="form-group">--}}
{{--                        <label for="pwd">Program Length:</label>--}}
{{--                        <select class="form-control" name="program_level">--}}
{{--                            <option value="Grade 1">Postgraduate Certificate / Master's Degree</option>--}}
{{--                            <option value="Grade 2">3-Year Bachelor's Degree</option>--}}
{{--                            <option value="Grade 3">4-Year Bachelor's Degree</option>--}}
{{--                            <option value="Grade 3">1 year master's degree</option>--}}
{{--                            <option value="Grade 3">3-9 month ESL program</option>--}}
{{--                            <option value="Grade 3">6 month college certificate</option>--}}
{{--                            <option value="Grade 3">7 month college diploma</option>--}}
{{--                            <option value="Grade 3">2 year college diploma including a practicum</option>--}}
{{--                            <option value="Grade 3">1 year college diploma</option>--}}
{{--                            <option value="Grade 3">13 month college diploma including a practicum</option>--}}
{{--                            <option value="Grade 3">2 year college attestation including internship</option>--}}
{{--                            <option value="Grade 3">9 month ESL program</option>--}}
{{--                            <option value="Grade 3">10 month college diploma</option>--}}
{{--                            <option value="Grade 3">3-week ESL program</option>--}}
{{--                            <option value="Grade 3">8 month college diploma</option>--}}
{{--                            <option value="Grade 3">8 month college diploma including a practicum</option>--}}
{{--                            <option value="Grade 3">9 month college diploma</option>--}}
{{--                            <option value="Grade 3">6 month college diploma including a practicum</option>--}}
{{--                            <option value="Grade 3">23 month college diploma</option>--}}
{{--                            <option value="Grade 3">5 month college diploma</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                    <button type="submit" class="btn btn-success">Submit</button>
                    <br>
                    <br>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('js')

@endpush
