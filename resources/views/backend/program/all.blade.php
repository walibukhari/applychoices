@extends('layouts.adminDefault')


@push('css')
<link href="{{asset('newasset/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('newasset/css/rowReorder.dataTables.min.css')}}" rel="stylesheet" type="text/css">
@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
         <a href="{{route('programFile')}}" class="btn btn-outline-info btn-lg" style="display: flex;
             position: absolute;
             right: 50px;
             height: 50px;">
             <span class="glyphicon glyphicon-export"></span> Export
            </a>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
            <li class="breadcrumb-item active">
                <a href="javascript:;" class="btn btn-sm btn-outline-info" onclick="window.location.href='{{route('program.management')}}'">
                    Add Program
                </a>
            </li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('program_message'))
                    <div class="alert alert-success">
                        {{session()->get('program_message')}}
                    </div>
                @endif
                <div class="table-responsive">
                   <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>School</th>
                                <th>Application Fee</th>
                                <th>Description</th>
                                <th>Program Level</th>
                                <th>Program Length</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                       <tbody>
                        @foreach($program as $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{$data->program_name}}</td>
                                <td>{{$data->school->name}}</td>
                                <td>{{$data->application_fee}}</td>
                                <td>{{$data->description}}</td>
                                <td>{{isset($data->program_level) ? $data->program_level : '--'}}</td>
                                <td>{{$data->program_length}}</td>
                                <td>
                                    <a href="{{route('edit.program',[$data->id])}}" class="btn btn-outline-secondary">Edit</a>
                                    <a href="{{route('delete.program',[$data->id])}}" class="btn btn-outline-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
<script src="{{asset('newasset/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('newasset/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('newasset/js/dataTables.rowReorder.min.js')}}"></script>
    <script src="{{asset('newasset/js/dataTables.responsive.min.js')}}"></script>
<script>

    $(document).ready(function() {

        $('#example').dataTable({
            searching: true,
            "sDom": '<"row view-filter"<"col-sm-12"<"pull-right"l><"pull-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
        });


        $('#example-1').dataTable({
            searching: false,
        });

    });
</script>

@endpush
