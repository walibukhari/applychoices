@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
{{--            <li class="breadcrumb-item active">Create School</li>--}}
            <li class="breadcrumb-item active">
                Transactions
            </li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('success_msg'))
                    <div class="alert alert-success">
                        {{session()->get('success_msg')}}
                    </div>
                @endif

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Email Address</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($transaction as $trans)
                                <tr>
                                    <td>{{$trans->id}}</td>
                                    <td>{{$trans->user->name}}</td>
                                    <td>{{$trans->amount}}</td>
                                    <td>{{\Carbon\Carbon::parse($trans->created_at)}}</td>
                                    <td>{{$trans->user->email}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')

@endpush
