@extends('layouts.default')

@push('css')
    <style>
        .AccountForm input[type='email'] {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
        .AccountForm input[type='password'] {
            margin: 0;
            padding: 0 20px;
            background: #fff;
            border-radius: 50px;
            border: none;
            outline: none;
            font-family: 'Eina03-Regular';
            font-size: 17px;
            text-align: left;
            text-decoration: none;
            width: 100%;
            height: 50px;
        }
        .navbar{
            display: none !important;
        }
    </style>
@endpush

@section('content')
@if(session()->has('error_ad'))
    <div class="alert alert-danger" style="margin-bottom: 0px;">
        {{session()->get('error_ad')}}
    </div>
@endif
<section class="Section-AccountRegister">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="AccountHead">
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s">Admin Login
{{--                        <br /> Applying to Schools</h1>--}}
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-3 col-md-3 col-12"></div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <form method="POST" action="{{ route('admin.post.login') }}">
                            @csrf
                        <div class="AccountForm wow fadeInUp" data-wow-delay="0.2s">
                            <label>Email</label>
                            <input type="email" id="" required name="email" placeholder="enter email address">
                        </div>

                        <div class="AccountForm wow fadeInUp" data-wow-delay="0.4s">
                            <label>Password</label>
                            <input type="password" id="" required name="password" placeholder="enter password">
                        </div>

                        <div class="AccountForm wow fadeInUp" data-wow-delay="0.8s">
                            <button type="submit">Login</button>
                        </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-xl-3 col-md-3 col-12"></div>

                </div>
            </div>

        </div>
    </div>
</section>
@endsection

@push('js')
@endpush
