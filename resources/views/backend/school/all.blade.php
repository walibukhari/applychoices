@extends('layouts.adminDefault')


@push('css')
    <link href="{{asset('newasset/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('newasset/css/rowReorder.dataTables.min.css')}}" rel="stylesheet" type="text/css">
@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
         <a href="{{route('excelFile')}}" class="btn btn-outline-info btn-lg" style="display: flex;
             position: absolute;
             right: 50px;
             height: 50px;">
             <span class="glyphicon glyphicon-export"></span> Export
            </a>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">All Schools</li>
            <li class="breadcrumb-item active">
                <a href="javascript:;" class="btn btn-sm btn-outline-info" onclick="window.location.href='{{route('school.management')}}'">
                    Add School
                </a>
            </li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('school_message'))
                    <div class="alert alert-success">
                        {{session()->get('school_message')}}
                    </div>
                @endif
                <div class="table-responsive">
                   <table class="table table-striped" id="example1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Logo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                       <tbody>
                        @foreach($data as $school)
                            <tr>
                                <td>{{$school->id}}</td>
                                <td>{{$school->name}}</td>
                                <td>{{$school->country.' '.$school->state.' '.$school->place}}</td>
                                <td><img src="{{$school->logo_link}}" style="width: 50px;" /></td>
                                <td>
                                    <a href="{{route('edit.school',[$school->id])}}" class="btn btn-outline-secondary">Edit</a>
                                    <a href="{{route('delete.school',[$school->id])}}" class="btn btn-outline-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
<script src="{{asset('newasset/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('newasset/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('newasset/js/dataTables.rowReorder.min.js')}}"></script>
    <script src="{{asset('newasset/js/dataTables.responsive.min.js')}}"></script>
<script>

    $(document).ready(function() {

        $('#example1').dataTable({
            searching: true,
            "sDom": '<"row view-filter"<"col-sm-12"<"pull-right"l><"pull-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
        });


        $('#example-1').dataTable({
            searching: false,
        });

    });
</script>

@endpush
