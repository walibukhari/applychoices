@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Create School</li>
            <li class="breadcrumb-item active">
                <a href="javascript:;" class="btn btn-sm btn-outline-info" onclick="window.location.href='{{route('all.school')}}'">
                    All School
                </a>
            </li>
        </ol>
        <div class="row">
           <div class="col-md-12">
               @if(session()->has('success_message'))
                   <div class="alert alert-success">
                       {{session()->get('success_message')}}
                   </div>
               @endif
               <form action="{{route('store.school')}}" method="POST">
                   @csrf
                   <div class="form-group">
                       <label for="email">Name:</label>
                       <input type="text" name="name" required class="form-control" id="email">
                   </div>
                   <div class="form-group">
                       <label for="pwd">Country:</label>
                       <input type="text" class="form-control" required name="country">
                   </div>
                   <div class="form-group">
                       <label for="pwd">State:</label>
                       <input type="text" class="form-control" required name="state">
                   </div>
                   <div class="form-group">
                       <label for="pwd">Place:</label>
                       <input type="text" class="form-control" name="place">
                   </div>
                   <div class="form-group">
                       <label for="pwd">Logo:</label>
                       <input type="file" name="file" class="form-control" />
                   </div>
                   <div class="form-group">
                       <label for="pwd">Logo URL :</label>
                       <input type="text" name="logo_link" class="form-control" required />
                   </div>
                   <button type="submit" class="btn btn-success">Submit</button>
                   <br>
                   <br>
               </form>
           </div>
        </div>
    </div>
@endsection


@push('js')

@endpush
