@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Edit School</li>
        </ol>
        <div class="row">
           <div class="col-md-12">
               @if(session()->has('success_message'))
                   <div class="alert alert-success">
                       {{session()->get('success_message')}}
                   </div>
               @endif
               <form action="{{route('update.school')}}" method="POST">
                   @csrf
                   <input type="hidden" name="id" value="{{$school->id}}" />
                   <div class="form-group">
                       <label for="email">Name:</label>
                       <input type="text" name="name" value="{{$school->name}}" required class="form-control" id="email">
                   </div>
                   <div class="form-group">
                       <label for="pwd">Country:</label>
                       <input type="text" class="form-control" value="{{$school->country}}" required name="country">
                   </div>
                   <div class="form-group">
                       <label for="pwd">State:</label>
                       <input type="text" class="form-control" value="{{$school->state}}" required name="state">
                   </div>
                   <div class="form-group">
                       <label for="pwd">Place:</label>
                       <input type="text" class="form-control" value="{{$school->place}}" name="place">
                   </div>
                   <div class="form-group">
                       <label for="pwd">Logo:</label>
                       <input type="file" name="file" class="form-control" />
                   </div>
                   <div class="form-group">
                       <label for="pwd">Logo URL :</label>
                       <input type="text" name="logo_link" value="{{$school->logo_link}}" class="form-control" required />
                   </div>
                   <button type="submit" class="btn btn-success">Update</button>
                   <br>
                   <br>
               </form>
           </div>
        </div>
    </div>
@endsection


@push('js')

@endpush
