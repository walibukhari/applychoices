@extends('layouts.adminDefault')


@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">Add School</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="{{route('school.management')}}">View Details</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body">Add Program</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="{{route('program.management')}}">View Details</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-success text-white mb-4">
                    <div class="card-body">Chat With Recruiters</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="{{route('chat.views')}}">View Details</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-danger text-white mb-4">
                    <div class="card-body">Chat With Users</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" href="{{route('adminChat')}}">View Details</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-area mr-1"></i>
                        Apply Choices Chart
                    </div>
                    <div class="card-body" style="height: 370px; width: 100%;" id="chartContainer"></div>
                </div>
            </div>
{{--            <div class="col-xl-12">--}}
{{--                <div class="card mb-4">--}}
{{--                    <div class="card-header">--}}
{{--                        <i class="fas fa-chart-bar mr-1"></i>--}}
{{--                        Recruiters--}}
{{--                    </div>--}}
{{--                    <div class="card-body" id="chartdiv"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
@endsection


@push('js')
    @php
       $recuriter =  \App\User::where('register_as','=',\App\User::AS_RECRUITER)->get();
       $count = count($recuriter);
        $school_partners = \App\User::where('register_as','=',\App\User::AS_SCHOOL_PARTNER)->get();
        $school = count($school_partners);
    @endphp
{{--    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>--}}
{{--    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>--}}
{{--    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>--}}
{{--    <script>--}}
{{--        am4core.ready(function() {--}}

{{--// Themes begin--}}
{{--            am4core.useTheme(am4themes_animated);--}}
{{--// Themes end--}}

{{--// Create chart instance--}}
{{--            var chart = am4core.create("chartdiv", am4charts.XYChart);--}}

{{--// Add data--}}
{{--            chart.data = [{--}}
{{--                "country": "Total Recruiters",--}}
{{--                "visits": '{{$count}}'--}}
{{--            }];--}}

{{--// Create axes--}}

{{--            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());--}}
{{--            categoryAxis.dataFields.category = "country";--}}
{{--            categoryAxis.renderer.grid.template.location = 0;--}}
{{--            categoryAxis.renderer.minGridDistance = 30;--}}

{{--            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {--}}
{{--                if (target.dataItem && target.dataItem.index & 2 == 2) {--}}
{{--                    return dy + 25;--}}
{{--                }--}}
{{--                return dy;--}}
{{--            });--}}

{{--            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());--}}

{{--// Create series--}}
{{--            var series = chart.series.push(new am4charts.ColumnSeries());--}}
{{--            series.dataFields.valueY = "visits";--}}
{{--            series.dataFields.categoryX = "country";--}}
{{--            series.name = "Visits";--}}
{{--            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";--}}
{{--            series.columns.template.fillOpacity = .8;--}}

{{--            var columnTemplate = series.columns.template;--}}
{{--            columnTemplate.strokeWidth = 2;--}}
{{--            columnTemplate.strokeOpacity = 1;--}}

{{--        }); // end am4core.ready()--}}
{{--    </script>--}}
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    <script>
        window.onload = function() {

            var options = {
                title: {
                    text: "ApplyChoices"
                },
                data: [{
                    type: "pie",
                    startAngle: 45,
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabel: "{label} ({y})",
                    yValueFormatString:"#,##0.#"%"",
                    dataPoints: [
                        { label: "Recruiters", y: '{{$count}}' },
                        { label: "School Partners", y: '{{$school}}' },
                    ]
                }]
            };
            $("#chartContainer").CanvasJSChart(options);
        }
    </script>
{{--<script>--}}
{{--    // Set new default font family and font color to mimic Bootstrap's default styling--}}
{{--    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';--}}
{{--    Chart.defaults.global.defaultFontColor = '#292b2c';--}}
{{--    // Area Chart Example--}}
{{--    let arr = JSON.toString('{{$school_partners}}');--}}
{{--    console.log(arr);--}}
{{--    var ctx = document.getElementById("myAreaChart");--}}
{{--    var myLineChart = new Chart(ctx, {--}}
{{--        type: 'line',--}}
{{--        data: {--}}
{{--            labels: [],--}}
{{--            datasets: [{--}}
{{--                label: "Sessions",--}}
{{--                lineTension: 0.3,--}}
{{--                backgroundColor: "rgba(2,117,216,0.2)",--}}
{{--                borderColor: "rgba(2,117,216,1)",--}}
{{--                pointRadius: 5,--}}
{{--                pointBackgroundColor: "rgba(2,117,216,1)",--}}
{{--                pointBorderColor: "rgba(255,255,255,0.8)",--}}
{{--                pointHoverRadius: 5,--}}
{{--                pointHoverBackgroundColor: "rgba(2,117,216,1)",--}}
{{--                pointHitRadius: 50,--}}
{{--                pointBorderWidth: 2,--}}
{{--                data: [10000, 30162, 26263, 18394, 18287, 28682, 31274, 33259, 25849, 24159, 32651, 31984, 38451],--}}
{{--            }],--}}
{{--        },--}}
{{--        options: {--}}
{{--            scales: {--}}
{{--                xAxes: [{--}}
{{--                    time: {--}}
{{--                        unit: 'date'--}}
{{--                    },--}}
{{--                    gridLines: {--}}
{{--                        display: false--}}
{{--                    },--}}
{{--                    ticks: {--}}
{{--                        maxTicksLimit: 7--}}
{{--                    }--}}
{{--                }],--}}
{{--                yAxes: [{--}}
{{--                    ticks: {--}}
{{--                        min: 0,--}}
{{--                        max: 40000,--}}
{{--                        maxTicksLimit: 5--}}
{{--                    },--}}
{{--                    gridLines: {--}}
{{--                        color: "rgba(0, 0, 0, .125)",--}}
{{--                    }--}}
{{--                }],--}}
{{--            },--}}
{{--            legend: {--}}
{{--                display: false--}}
{{--            }--}}
{{--        }--}}
{{--    });--}}

{{--</script>--}}
@endpush
