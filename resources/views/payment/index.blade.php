<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 100px;
        }
    </style>
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                    <div class="row display-tr" >
                        <h3 class="panel-title display-td" >Payment Details</h3>
                        <div class="display-td" >
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>
                </div>
                <div class="panel-body">

                    @if (Session::has('success_msg'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success_msg') }}</p>
                        </div>
                    @endif

                <form role="form" action="{{route('render.stores')}}" method="POST" class="require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                    id="payment-form">
                        @csrf

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control' size='4' type='text' name = "name_of_card" id = "name_of_card">
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text' name = "card_number" id = "card_number">
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text' name="cvs_number" id="cvs_number">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text' name ="expiration_month" id ="expiration_month">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text' name="expiration_year" id="expiration_year">
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now ($100)</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="ui-widget" style="width: 20%;">
    <label for="tags" style="float:left; margin-right:5px; font-size:12px; margin-top:10px; margin-left:55px; text-align:right;">Search Engines:</label>
    <input type="text" placeholder="Search for engine..." id="tags" style="width:150px; padding:3px; margin:9px 0 0 0; float:right;" />
</div>
<div class="ui-widget" style="width: 20%;">
    <label for="tags" style="float:left; margin-right:5px; font-size:12px; margin-top:10px; margin-left:55px; text-align:right;">Search Engines:</label>
    <input type="text" placeholder="Search for engine..." id="tags2" style="width:150px; padding:3px; margin:9px 0 0 0; float:right;" />
</div>
<br>
<br>
<br><br>
<br>
<br>
<br>
<br><br>
<br>
<br>
<br>
<br><br>



</body>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
$(function() {


    var route1 = "{{ url('/get/autocomplete/data') }}";
    var route = "{{ url('/get/autocomplete/location/data') }}";

    $(function() {
        // var data = [{
        //     label: "Apple",
        //     value: "http://www.apple.com"},
        //     {
        //         label: "Google",
        //         value: ""
        //     },
        //     {
        //         label: "Yahoo",
        //         value: ""},
        //     {
        //         label: "Bing",
        //         value: ""},
        //     {
        //         label: "bingo",
        //         value: ""},
        //     {
        //         label: "bingoo",
        //         value: ""},
        //     {
        //         label: "Biaaa",
        //         value: ""}
        // ];
// $('#search').typeahead({
        //     source: function (query, process) {
        //         states = [];
        //         map = {};
        //         $.get(route1, { term: query }, function (data) {
        //             console.log('data');
        //             console.log(data);
        //             $.each(data, function (i, state) {
        //                 map[state.program_name] = state;
        //                 states.push(state.program_name);
        //             });
        //         });
        //         setTimeout(() => {
        //             console.log('data datadata data data data');
        //             console.log(states);
        //             process(states);
        //         },500);
        //     },
        //     highlighter: function (item) {
        //         //If it's our link than just return the text in bold
        //         if (item == 'View All')
        //             return '<strong>' + item + '</strong>'
        //
        //         //The rest is the default hightlighter function taken from bootstrap's js source
        //         var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
        //         return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
        //             return '<strong>' + match + '</strong>'
        //         });
        //     },
        //     select: function( ui, item ) {
        //         console.log('item');
        //         console.log(item);
        //     }
        // });
        {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.11.0/jquery.typeahead.min.css" integrity="sha512-7zxVEuWHAdIkT2LGR5zvHH7YagzJwzAurFyRb1lTaLLhzoPfcx3qubMGz+KffqPCj2nmfIEW+rNFi++c9jIkxw==" crossorigin="anonymous" />--}}
        {{--<script src="https://cdn.jsdelivr.net/npm/bootstrap-typeahead@2.3.2/bootstrap-typeahead.min.js"></script>--}}
        var data = [];

        $.ajax({
            url:route1,
            type:'GET',

            success: function (response) {
                console.log('response');
                console.log(response);
                response.map((item) => {
                   console.log('data');
                   console.log(item.program_name);
                   data.push({label:item.program_name,value:''})
                });

            },
            error: function (error) {
                console.log('error');
                console.log(error);
            }
        })

        $( "#tags" ).autocomplete({
            source: function( request, response ) {
                var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
                response( $.grep( data, function( item ){
                    return matcher.test( item.label );
                }) );
            },
            minLength: 1,
            select: function(event, ui) {
                event.preventDefault();
                $("#tags").val(ui.item.label);
                $("#selected-tag").val(ui.item.label);
                // window.location.href = ui.item.value;
            }
            ,
            focus: function(event, ui) {
                event.preventDefault();
                $("#tags").val(ui.item.label);
            }
        });

        $.ajax({
            url:route,
            type:'GET',

            success: function (response) {
                console.log('response');
                console.log(response);
                response.map((item) => {
                    console.log('data');
                    console.log(item.name);
                    data.push({label:item.name,value:''})
                    data.push({label:item.country,value:''})
                });

            },
            error: function (error) {
                console.log('error');
                console.log(error);
            }
        })

        $( "#tags2" ).autocomplete({
            source: function( request, response ) {
                var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
                response( $.grep( data, function( item ){
                    return matcher.test( item.label );
                }) );
            },
            minLength: 1,
            select: function(event, ui) {
                event.preventDefault();
                $("#tags2").val(ui.item.label);
                $("#selected-tag2").val(ui.item.label);
                // window.location.href = ui.item.value;
            }
            ,
            focus: function(event, ui) {
                event.preventDefault();
                $("#tags2").val(ui.item.label);
            }
        });

        // $('#search2').typeahead({
        //     source: function (query, process) {
        //         states = [];
        //         map = {};
        //         $.get(route, { term: query }, function (data) {
        //             console.log('data');
        //             console.log(data);
        //             $.each(data, function (i, state) {
        //                 var dataState = state.name + ' , ' + state.country;
        //                 map[dataState] = state;
        //                 states.push(dataState);
        //             });
        //         });
        //         setTimeout(() => {
        //             console.log('data datadata data data data');
        //             console.log(states);
        //             process(states);
        //         },500);
        //     },
        //     highlighter: function (item) {
        //         //If it's our link than just return the text in bold
        //         if (item == 'View All')
        //             return '<strong>' + item + '</strong>'
        //
        //         //The rest is the default hightlighter function taken from bootstrap's js source
        //         var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
        //         return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
        //             return '<strong>' + match + '</strong>'
        //         });
        //     },
        //     select: function( ui, item ) {
        //         console.log('item');
        //         console.log(item);
        //     }
        // });
    });






    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');

        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });

    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }

  });

  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }

});
</script>
</html>
