<header class="HeaderTop-Nav">
    <nav>
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-3 col-12">
                    <div class="Main-logo">
                        <a href="{{route('homePage')}}">
                            <img src="{{asset('assets/image/Main-Logo.png')}}" width="351" height="52" alt=""/>
                        </a>
                    </div>
                </div>

                <div class="col-lg-9 col-md-9 col-12">
                    <div class="navbar navbar-expand-lg navbar-light">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse NavTop-Mt" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">

                                <li class="nav-item">
                                    <a class="nav-link hvr-bounce-to-bottom" href="{{route('homePage')}}">Home</a>
                                </li>

                                <li class="nav-item dropdown active">
                                    <a class="nav-link hvr-bounce-to-bottom dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Services
                                    </a>
                                    <div class="dropdown-menu fade-up" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{route('studentPage')}}">Students</a>
                                        <a class="dropdown-item" href="{{route('schoolPartner')}}">School Partner</a>
                                        @if(!\Auth::check())
                                            <a class="dropdown-item" href="{{route('agentPage')}}">Recruiter</a>
                                        @else
                                            @if(\Auth::user()->register_as == \App\User::AS_RECRUITER)
                                                <a class="dropdown-item" href="{{route('agentDashboard')}}">Recruiter</a>
                                            @else
                                                <a class="dropdown-item" href="{{route('agentPage')}}">Recruiter</a>
                                            @endif
                                        @endif
                                    </div>
                                </li>

{{--                                <li class="nav-item dropdown">--}}
{{--                                    <a class="nav-link hvr-bounce-to-bottom dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                        Countries--}}
{{--                                    </a>--}}
{{--                                    <div class="dropdown-menu fade-down" aria-labelledby="navbarDropdown2">--}}
{{--                                        <a class="dropdown-item" href="#">Action</a>--}}
{{--                                        <a class="dropdown-item" href="#">Another action</a>--}}
{{--                                    </div>--}}
{{--                                </li>--}}

                                <li class="nav-item dropdown">
                                    <a class="nav-link hvr-bounce-to-bottom dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        About
                                    </a>
                                    <div class="dropdown-menu fade-up" aria-labelledby="navbarDropdown3">
                                        <a class="dropdown-item" href="#">Events</a>
                                        <a class="dropdown-item" href="#">Contact Us</a>
                                    </div>
                                </li>
                                @if(\Auth::check())
                                    <li class="nav-item dropdown">
                                        <a class="nav-link hvr-bounce-to-bottom dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                           href="{{route('user.profile')}}" role="button"
                                         >
                                            <img src="{{asset('assets/image/256-512.webp')}}" width="20" style="height: auto;" alt=""/>
                                        </a>
                                        <div style="    min-width: 104px;" class="dropdown-menu fade-up" aria-labelledby="navbarDropdown4">
                                        @if(\Auth::user()->register_as == \App\User::AS_RECRUITER)
                                            <a class="dropdown-item" href="{{route('agentDashboard')}}">Dashboard</a>
                                            <a class="dropdown-item" href="{{route('user.profile')}}">profile</a>
                                        @else
                                            <a class="dropdown-item" href="{{route('user.profile')}}">profile</a>
                                        @endif
                                        <a class="dropdown-item" href="{{route('userLogout')}}">logout</a>
                                    </div>
                                    </li>
                                @endif
{{--                                <li class="nav-item dropdown">--}}
{{--                                    <a class="nav-link hvr-bounce-to-bottom dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                        Blogs--}}
{{--                                    </a>--}}
{{--                                    <div class="dropdown-menu fade-down" aria-labelledby="navbarDropdown4">--}}
{{--                                        <a class="dropdown-item" href="#">Action</a>--}}
{{--                                        <a class="dropdown-item" href="#">Another action</a>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
                            @if(!\Auth::check())
                                <div class="LoginBtn-Container">
                                    <a href="{{route('login')}}">Login</a>
                                    <a href="{{route('register')}}" class="active">Sign Up</a>
                                </div>
                            @else
                                    @if(\Auth::user()->register_as == \App\User::AS_RECRUITER)
                                    <!--<div class="LoginBtn-Container">-->
                                    <!--    <a href="{{route('recruiterLogout')}}">Logout</a>-->
                                    <!--</div>-->
                                    @else
                                    <div style="display: none;" class="LoginBtn-Container">
                                        <a href="{{route('userLogout')}}">Logout</a>
                                    </div>
                                    @endif
                                @endif
                            </ul>



                        </div>

                    </div>
                </div>

            </div>
        </div>
    </nav>
</header>
