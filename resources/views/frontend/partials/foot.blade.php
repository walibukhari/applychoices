<footer class="SectionFooter">

    <div class="container">
        <div class="row">

            <div class="col-xl-3 col-md-3 col-12">

                <div class="FooterHead"><h1>Services</h1></div>

                <div class="FooterLinks">
                    <ul>
                        <li><a href="{{route('studentPage')}}">Students</a></li>
                        <li><a href="{{route('schoolPartner')}}">Schools</a></li>
                        <li><a href="{{route('agentPage')}}">Recruiter</a></li>
                    </ul>
                </div>

                <div class="GetSocial-Media">
                    <h2>Get Social</h2>
                    <ul>
                        <li><a href="#" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                    </ul>
                </div>


            </div>

            <div class="col-xl-3 col-md-3 col-12">

                <div class="FooterHead"><h1>About</h1></div>

                <div class="FooterLinks">
                    <ul>
                        <li><a href="#">Our Story</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Press</a></li>
                        <li><a href="#">Life</a></li>
                        <li><a href="#">Leadership</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>

            </div>

            <div class="col-xl-3 col-md-3 col-12">

                <div class="FooterHead"><h1>Resources</h1></div>

                <div class="FooterLinks">
                    <ul>
                        <li><a href="#">Events & Webinars</a></li>
                        <li><a href="#">ApplyChoices Fees</a></li>
                        <li><a href="#">Discover Programs</a></li>
                        <li><a href="#">Discover Schools</a></li>
                        <li><a href="#">Register</a></li>
                    </ul>
                </div>

            </div>

            <div class="col-xl-3 col-md-3 col-12">

                <div class="FooterHead"><h1>Legal</h1></div>

                <div class="FooterLinks">
                    <ul>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Terms of Use</a></li>
                    </ul>
                </div>

            </div>

        </div>
    </div>

    <div class="Section-CopyRight">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-12">
                    <div class="CopyRight-Head">
                        <p>© 2020 ApplyChoices.com</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</footer>
