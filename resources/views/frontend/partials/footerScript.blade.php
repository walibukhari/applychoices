<script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('assets/owl carousel/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/select-chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/select-input.js')}}"></script>

<script>
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 50,
        mobile: false
    });
    wow.init();
</script>

<script>

    $(document).ready(function(){
        $("#Branding-Slider").owlCarousel({
            items:6,
            itemsDesktop:[1000,6],
            itemsDesktopSmall:[979,4],
            itemsTablet:[768,3],
            pagination:false,
            navigation:true,
            transitionStyle:"backSlide",
            navigationText:["<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"],
            autoPlay:true
        });
    });

</script>
