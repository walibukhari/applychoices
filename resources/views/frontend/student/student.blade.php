@extends('layouts.default')

@push('css')
    <style>
        .customStyle{
            margin: 0;
            padding: 10px 45px;
            font-family: 'Eina03-SemiBold';
            font-size: 20px;
            text-align: center;
            text-decoration: none;
            background: #80011f;
            border-radius: 50px;
            outline: none;
            color: #fff;
            display: inline-block;
            letter-spacing: 2px;
            text-decoration: none !important;
        }
        .customStyle:hover{
            background: #DAA520;
            color:#fff;
        }
    </style>
@endpush


@section('content')

<section class="AboutBanner">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-12">
                <div class="BannerHead">
                    <h1 class="wow fadeInUp" data-wow-delay="0.2s">Apply to <span>Study Abroad</span> <br />at Your Dream School</h1>
                    <p class="wow fadeInUp" data-wow-delay="0.4s">Learn about programs and schools, get <br />matched to the best options for you, easily <br />submit your application, and get the support<br /> you need along the way!</p>
                    <a class="wow fadeInUp" data-wow-delay="0.6s" href="{{route('searchPage')}}">Get Started</a>
                </div>
            </div>

        </div>
    </div>
</section>


<section class="Section-ThumbleAbout">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <h4 class="wow fadeInUp" data-wow-delay="0.2s">Study with ApplyChoices</h4>
            </div>

            <div class="col-lg-4 col-md-4 col-12">
                <div class="AboutUs-Thumble wow fadeInUp" data-wow-delay="0.2s">
                    <h1>High School</h1>
                    <p>High school typically includes grade 9-12 and allows students to develop skills to pursue higher education.</p>
                    <a href="{{url('search?filter=schools')}}">Explore High School</a>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-12">
                <div class="AboutUs-Thumble wow fadeInUp" data-wow-delay="0.4s">
                    <h1>Undergraduate</h1>
                    <p>High school typically includes grade 9-12 and allows students to develop skills to pursue higher education.</p>
                    <a href="{{url('search?filter=under_graduate')}}">Undergraduate Programs</a>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-12">
                <div class="AboutUs-Thumble wow fadeInUp" data-wow-delay="0.6s">
                    <h1>Post-Graduate</h1>
                    <p>High school typically includes grade 9-12 and allows students to develop skills to pursue higher education.</p>
                    <a href="{{url('search?filter=post_graduate')}}">Post-Graduate Programs</a>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="Section-HowitWorks">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="HowitWorks-Head">
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s">How It Works</h3>
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="main-timeline wow fadeInUp" data-wow-delay="0.4s">

                    <div class="timeline">
                        <a href="#" class="timeline-content">

                            <span class="timeline-year">Step 1</span>
                            <h3 class="title">Find the Right Program and School</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer males uada tellus lorem, et condimentum neque commodo
                            </p>

                        </a>
                    </div>

                    <div class="timeline">
                        <a href="#" class="timeline-content">

                            <span class="timeline-year">Step 2</span>
                            <h3 class="title">Submit Your Application</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer males uada tellus lorem, et condimentum neque commodo
                            </p>

                        </a>
                    </div>

                    <div class="timeline">
                        <a href="#" class="timeline-content">

                            <span class="timeline-year">Step 3</span>
                            <h3 class="title">Get Your Letter of Acceptance</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer males uada tellus lorem, et condimentum neque commodo
                            </p>

                        </a>
                    </div>

                    <div class="timeline">
                        <a href="#" class="timeline-content">

                            <span class="timeline-year">Step 4</span>
                            <h3 class="title">Start the Visa Process</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer males uada tellus lorem, et condimentum neque commodo
                            </p>

                        </a>
                    </div>

                    <div class="timeline">
                        <a href="#" class="timeline-content">

                            <span class="timeline-year">Step 5</span>
                            <h3 class="title">Book Your Plane Ticket and Go!</h3>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer males uada tellus lorem, et condimentum neque commodo
                            </p>

                        </a>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<section class="Section-AboutSlider">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div id="AboutUs-slider" class="owl-carousel wow fadeInUp" data-wow-delay="0.4s">

                    <div class="testimonial">

                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>

                        <div class="testimonial-content">
                            <h3 class="title">- Bahri S., International Student at Conestoga College</h3>
                        </div>


                    </div>

                    <div class="testimonial">

                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>

                        <div class="testimonial-content">
                            <h3 class="title">- Bahri S., International Student at Conestoga College</h3>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="Section-FAQ">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-sm-12">
                <h1 class="wow fadeInUp" data-wow-delay="0.2s">Frequently Asked Questions (FAQ)</h1>
                <h2 class="wow fadeInUp" data-wow-delay="0.4s">General Information</h2>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="panel-group wow fadeInUp" data-wow-delay="0.6s" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">

                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Why should I study in Canada or the United States?
                                </a>
                            </h4>
                        </div>

                        <div id="collapseOne" class="panel-collapse collapse in show"
                             role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default">

                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    How much does tuition cost in the United States and Canada?
                                </a>
                            </h4>
                        </div>

                        <div id="collapseTwo" class="panel-collapse collapse"
                             role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default">

                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    What documents do I need to apply to a program?
                                </a>
                            </h4>
                        </div>

                        <div id="collapseThree" class="panel-collapse collapse"
                             role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
        <div class="row" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;    display: flex;
    justify-content: center;
    position: relative;
    top: 40px;">
            <a
               href="{{route('searchPage')}}" class="customStyle">
                Apply Now
            </a>
        </div>
    </div>
</section>

@endsection


@push('js')
<script>

    $(document).ready(function(){
        $("#AboutUs-slider").owlCarousel({
            items:1,
            itemsDesktop:[1000,2],
            itemsDesktopSmall:[979,2],
            itemsTablet:[768,1],
            pagination:true,
            autoPlay:true
        });
    });

</script>
@endpush
