@extends('layouts.default')

@push('css')

@endpush

@section('content')
@if(Session::has('success_msg'))
    <div class="alert alert-info" style="margin-bottom: 0px;">
        {{ Session::get('success_msg') }}
    </div>
@endif
<section class="SchoolBanner-Bg">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-12">
                <div class="School-BannerHead">
                    <h1 class="wow fadeInUp" data-wow-delay="0.2s">Increase Your Global Presence and</h1>
                    <h2 class="wow fadeInUp" data-wow-delay="0.4s">Diversify Your Campus</h2>
                    <p class="wow fadeInUp" data-wow-delay="0.6s">ApplyBoard recruits qualified international<br /> students from 110+ countries.</p>
                    <a class="wow fadeInUp" data-wow-delay="0.8s" href="#">Partner with Us</a>
                </div>
            </div>

        </div>
    </div>
</section>




<section class="Section-SchoolHead">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <h4 class="wow fadeInUp" data-wow-delay="0.4s">Expand Your International Reach</h4>
                <p class="wow fadeInUp" data-wow-delay="0.6s">Attract the best students from around the world with ApplyChoices.</p>
            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="serviceBox wow fadeInUp" data-wow-delay="0.2s">

                    <div class="service-icon">
                        <span><i class="fa fa-globe"></i></span>
                    </div>

                    <h3 class="title">Increase Student Diversity</h3>
                    <p class="description">Students of about more than 110 countries can apply to different universities. This platform changes the lives of millions of students every year which fulfills their needs, desires, and priorities. The platform of Apply Board permits you to enhance and expand your campus establishing a link with high competent students from all over the world. Different companies work together to increase the number of international students from the globe.</p>

                </div>
            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="serviceBox wow fadeInUp" data-wow-delay="0.4s">

                    <div class="service-icon">
                        <span><i class="fa fa-globe"></i></span>
                    </div>

                    <h3 class="title">Receive Quality Applications</h3>
                    <p class="description">You can get only highly qualified and those applications that are filled. You have to provide students with an extraordinary experience at more than 1200 different institutes of education with minimum cost and maximum efficiency. Through a variety of study platforms, a huge number of students apply for scholarships. Student portals is a prodigious source of information in this domain. You must make your institutions more efficient to attract many applications from all over the world.</p>

                </div>
            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="serviceBox wow fadeInUp" data-wow-delay="0.6s">

                    <div class="service-icon">
                        <span><i class="fa fa-globe"></i></span>
                    </div>

                    <h3 class="title">Approved Recruiter Network</h3>
                    <p class="description">To accomplish this mission, we are working in partnership with more than 4000 evaluated and proficient recruiters, school of law, school of business, and other institutions worldwide. These platforms are working very actively to enhance the network of recruiters globally.</p>

                </div>
            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="serviceBox wow fadeInUp" data-wow-delay="0.8s">

                    <div class="service-icon">
                        <span><i class="fa fa-globe"></i></span>
                    </div>

                    <h3 class="title">Document Verification</h3>
                    <p class="description">You must verify all documents of students with the step of procedure including all documents related to their education and IELTS as well. Without verification, you cannot further progress the procedure.</p>

                </div>
            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="serviceBox wow fadeInUp" data-wow-delay="0.10s">

                    <div class="service-icon">
                        <span><i class="fa fa-globe"></i></span>
                    </div>

                    <h3 class="title">Applicant Matching</h3>
                    <p class="description">Only those students can apply in our all programs who are eligible and capable and can perform their best.</p>

                </div>
            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="serviceBox wow fadeInUp" data-wow-delay="0.12s">

                    <div class="service-icon">
                        <span><i class="fa fa-globe"></i></span>
                    </div>

                    <h3 class="title">Promotional Channels</h3>
                    <p class="description">Promote and advertise your school on different platforms, on social media websites, and at different events. If you have better promotion techniques. The number of applicants will be increased, they will apply through different study portals to achieve their destinations.</p>

                </div>
            </div>

        </div>
    </div>
</section>

<div class="Section-LogoBranding">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="LogoBranding-Head wow fadeInUp"  data-wow-delay="0.2s">
                    <h4>Access to More <span>Opportunities</span></h4>
                    <p>What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing<br /> and typesetting industry Lorem Ipsum has been standard</p>
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div id="Branding-Slider" class="owl-carousel wow fadeInUp" data-wow-delay="0.3s">

                    <div class="LogoBrand-Image">
                        <a href="#" target="_blank"><img src="{{asset('assets/image/branding-01.png')}}" width="155" height="75" alt=""/></a>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-02.png')}}" width="247" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-03.png')}}" width="312" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-04.png')}}" width="340" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-05.png')}}" width="257" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-06.png')}}" width="1147" height="300" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-01.png')}}" width="155" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-02.png')}}" width="247" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-03.png')}}" width="312" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-04.png')}}" width="340" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-05.png')}}" width="257" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-06.png')}}" width="1147" height="300" alt=""/>
                    </div>

                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="SliderBtn">
                    <a href="#">Explore Institutions</a>
                </div>
            </div>

        </div>
    </div>
</div>

<section class="Section-SchoolHead">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <h4 class="wow fadeInUp" data-wow-delay="0.2s">The Benefits of Partnering<br /> with ApplyBoard</h4>
                <p class="wow fadeInUp" data-wow-delay="0.4s">ApplyBoard makes recruiting international students easier.</p>
            </div>

            <div class="col-xl-6 col-md-6 col-12">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.2s">
                    <h5>Spend Less Time Vetting Applications</h5>
                    <h6>What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has What industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown.</h6>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.4s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

        </div>
    </div>
</section>

<section class="Section-Campus">
    <div class="container">
        <div class="row">

            <div class="col-xl-6 col-md-6 col-12 d-none d-sm-block">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.2s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.4s">
                    <h5>Increase Your<br> Campus Diversity</h5>
                    <h6>Expand your international reach to students in 110+ countries, bringing new cultures and greater diversity to your campuses.</h6>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12 d-xl-none d-block d-sm-none">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.2s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

        </div>
    </div>
</section>

<section class="Section-SchoolHead">
    <div class="container">
        <div class="row">

            <div class="col-xl-6 col-md-6 col-12">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.2s">
                    <h5>Attract Engaged Students</h5>
                    <h6>Students are matched to the right institution and program for them, and also have the freedom to search based on their interests.</h6>
                    <a href="#">See The Student Journey</a>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12">
                <div class="VettingApp wow fadeInUp" data-wow-delay="0.4s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

        </div>
    </div>
</section>

<section class="Section-PartnerBackground">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="ContactPartner-Head">
                            <h1 class="wow fadeInUp" data-wow-delay="0.2s">WORK WITH APPLYCHOICES</h1>
                            <h2 class="wow fadeInUp" data-wow-delay="0.4s">Partnership Request</h2>
                            <p class="wow fadeInUp" data-wow-delay="0.6s">Complete the form below and our Partner<br> Relations Team will be in touch soon.</p>
                        </div>
                    </div>
    <form action="{{route('schools.store')}}" method="POST" class="form-horizental">
        @csrf
        <div class="container">
            <div class="row">
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.1s">
                            <select id="class_partners" name="class_partners" required="school_partners">
                                <option>Please Select</option>
                                <option id="1">1</option>
                                <option id="2">2</option>
                                <option id="3">3</option>
                                <option id="4">4</option>
                                <option id="5">5</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.2s">
                            <input type="text" id="school_name" name="school_name" placeholder="School Name*" required="school_name">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.3s">
                            <input type="text" id="contact_first_name" name="contact_first_name" placeholder="Contact First Name*" required="contact_first_name">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.4s">
                            <input type="text" id="contact_last_name" name="contact_last_name" placeholder="Contact Last Name*" required="contact_last_name">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.5s">
                            <input type="text" id="contact_email" name="contact_email" placeholder="Contact Email*" required="contact_email">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.6s">
                            <input type="number" id="phone_number" name="phone_number" placeholder="Phone Number" required="phone_number">
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.7s">
                            <input type="text" id="contact_title" name="contact_title" placeholder="Contact Title*" required="contact_title">
                        </div>
                    </div>

                    <div class="col-md-12 col-xl-12 col-12">
                        <label class="PartnerCheckbox wow fadeInUp" data-wow-delay="0.8s">
                            Check if you have been referred by someone in ApplyChoices
                            <input type="checkbox" checked="checked">
                            <span class="Partnercheckmark"></span>
                        </label>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="ContactPartner-Form wow fadeInUp" data-wow-delay="0.9s">
                            <textarea id="additional_partners" name="additional_partners" placeholder="Any additional comments:" required="additional_partners"></textarea>
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="PartnerBtn wow fadeInUp" data-wow-delay="0.10s">
                            <button type="submit">Submit</button>
                        </div>
                    </div>


                </div>
              </div>
            </div>
        </div>
</form>

        </div>
    </div>
</section>

<section class="Section-AboutSlider">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div id="AboutUs-slider" class="owl-carousel wow fadeInUp" data-wow-delay="0.4s">

                    <div class="testimonial">

                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>

                        <div class="testimonial-content">
                            <h3 class="title">- Bahri S., International Student at Conestoga College</h3>
                        </div>


                    </div>

                    <div class="testimonial">

                        <p class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>

                        <div class="testimonial-content">
                            <h3 class="title">- Bahri S., International Student at Conestoga College</h3>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="SectionEvents">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <h4 class="wow fadeInUp" data-wow-delay="0.2s">Events and Webinars</h4>
                <p class="wow fadeInUp" data-wow-delay="0.4s">Stay up-to-date on events we’re attending and webinars we’re hosting.</p>
                <a class="wow fadeInUp" data-wow-delay="0.6s" href="#">Learn More</a>
            </div>

        </div>
    </div>
</section>

@endsection

@push('js')
    <script>

        $(document).ready(function(){
            $("#AboutUs-slider").owlCarousel({
                items:1,
                itemsDesktop:[1000,2],
                itemsDesktopSmall:[979,2],
                itemsTablet:[768,1],
                pagination:true,
                autoPlay:true
            });
        });

    </script>

    <script>

        $(document).ready(function(){
            $("#Branding-Slider").owlCarousel({
                items:6,
                itemsDesktop:[1000,6],
                itemsDesktopSmall:[979,4],
                itemsTablet:[768,3],
                pagination:false,
                navigation:true,
                transitionStyle:"backSlide",
                navigationText:["<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"],
                autoPlay:true
            });
        });

    </script>
@endpush
