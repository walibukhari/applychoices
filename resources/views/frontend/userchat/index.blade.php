@extends('layouts.default')

@push('css')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/rowReorder.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/globel.css')}}" rel="stylesheet" type="text/css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
    	.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}
           .HeaderTop-Nav .navbar {
    padding: 0;
    float: right;
    margin-bottom: -4px;
}
.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
    overflow-y: scroll;
    height: 250px;
}

::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}
.panel-primary>.panel-heading+.panel-collapse>.panel-body {
    display: block !important;
    height: 342px;

}
.panel-primary {
    border-color: #DAA520 !important;
}
[v-cloak] {
	display: none;
}
        .AgentsProfile {
            margin: 0 auto;
            padding: 40px 0 50px 0px;
            text-align: center;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
    </style>

@endpush

@section('content')
<section class="Agents-BodyContent" id="recruiter" v-cloak>

    <div class="container-fluid">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="AgentsAccordion wow fadeInUp" data-wow-delay="0.4s">
                   	<div class="container" style="width: 100%;margin-top: 100px;">
       <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" id="accordion" style="display: flex;
    align-items: center;
    justify-content: space-between;background: #DAA520;">
                      <div>
                        <span class="glyphicon glyphicon-comment"></span> Chat
                      </div>

                        <button type="button" @click="clickToRefresh()" class="btn btn-warning" style="display: flex; align-items: center;">Click To Refresh Chat
                           <i class="fa fa-spinner" v-if="!spinLoader" style="position: relative;
                          left: 7px;" aria-hidden="true"></i>
                          <i class="fa fa-spinner fa-spin" v-if="spinLoader" style="position: relative;
                          left: 7px;" aria-hidden="true"></i>
                        </button>

                    </div>
                <div class="panel-collapse collapse show" aria-expanded="true">
                    <div class="panel-body">
                        <ul class="chat" v-for="messageData in allMessages">
                            <li class="left clearfix" v-if="messageData.send_from == 'Admin'">
                            	<span class="chat-img pull-left">
                                <img src="http://placehold.it/50/55C1E7/fff&text=Admin" alt="User Avatar" class="img-circle" />
                            </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <strong class="primary-font">@{{ messageData.send_from }}</strong> <small class="pull-right text-muted">
                                            <span class="glyphicon glyphicon-time"></span>@{{ getDate(messageData.created_at) }}</small>
                                    </div>
                                    <p v-if="messageData.file">
                                        <br>
                                        <a style="font-size: 15px;" :href="'/download/'+messageData.id">@{{ replaceName(messageData.file) }}</a>
                                        <br>
                                        <img :src="'/'+messageData.file"
                                             @click="downloadFile(messageData.id)"
                                             onerror="this.src='{{asset('document.png')}}'"
                                             style="width: 200px;cursor:pointer;height: 200px;object-fit: cover;" />
                                    </p>
                                    <p v-else>
                                        @{{ messageData.messages }}
                                    </p>
                                </div>
                            </li>
                            <li class="right clearfix" v-if="messageData.send_from != 'Admin'">
                            	<span class="chat-img pull-right">
                                <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                            </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>@{{ getDate(messageData.created_at) }}</small>
                                        <strong class="pull-right primary-font">
                                            @{{ messageData.send_from }}
                                        </strong>
                                    </div>
                                    <p v-if="messageData.file">
                                        <br>
                                        <a style="font-size: 15px;" :href="'/download/'+messageData.id">@{{ replaceName(messageData.file) }}</a>
                                        <br>
                                        <img :src="'/'+messageData.file"
                                             @click="downloadFile(messageData.id)"
                                             onerror="this.src='{{asset('document.png')}}'"
                                             style="width: 200px;cursor:pointer;height: 200px;object-fit: cover;" />
                                    </p>
                                    <p v-else>
                                        @{{ messageData.messages }}
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <input v-model="enterMessage" type="text" v-on:keyup.enter="onEnter" class="form-control input-sm" placeholder="Type your message here..." />
                            <span class="input-group-btn">
                                <button type="button" @click="uploadFile()" class="btn btn-warning btn-sm">
                                    <input type="file" name="file" id="getFile" @change="triggerImage($event)" style="display:none;" />
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                </button>
                                <button type="button" @click="submitMessage()" class="btn btn-warning btn-sm">
                                    Send
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    				</div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection

@push('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.rowReorder.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.responsive.min.js')}}"></script>
<script>

    $(document).ready(function() {

        $('#example').dataTable({
            searching: true,
            "sDom": '<"row view-filter"<"col-sm-12"<"pull-right"l><"pull-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
        });


        $('#example-1').dataTable({
            searching: false,
        });

    });

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js" integrity="sha512-Izh34nqeeR7/nwthfeE0SI3c8uhFSnqxV0sI9TvTcXiFJkMd6fB644O64BRq2P/LA/+7eRvCw4GmLsXksyTHBg==" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js" integrity="sha512-v8ng/uGxkge3d1IJuEo6dJP8JViyvms0cly9pnbfRxT6/31c3dRWxIiwGnMSWwZjHKOuY3EVmijs7k1jz/9bLA==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js" integrity="sha512-wGKmIfDWUJSUvxUfUayQPJj7ADCD60La3up0VCbq+MTFcOUQ2hlH2McnYFafHgLTsOrGwOdiHKX4p1v0BerCyQ==" crossorigin="anonymous"></script>
<script type="text/javascript">
	var app = new Vue({
  el: '#recruiter',
  data: {
    message: 'Hello Vue!',
    recruiter : [],
    username:'',
    recruiter_id:'',
    allMessages:[],
    recruiterId:'{{ \Auth::user()->id }}',
    recruiter_name:'{{ \Auth::user()->name }}',
    recruiter_image:'{{ \Auth::user()->image }}',
    enterMessage:'',
    adminID:1,
    spinLoader:false,
    hoverTrue:false

  },
  methods:{
      downloadFile: function (id){
          window.location.href='/download/'+id;
      },
      replaceName: function (name){
          let fileName = name.split('chat/');
          return fileName[1];
      },
      uploadFile: function (){
            $('#getFile').click();
      },
      onEnter: function () {
          this.submitMessage();
      },
    uploadImage: function(){
        $('#uploadImage').click();
    },
      getDate: function (date) {
          return moment(date).format('MM/DD/YYYY , h:mm:ss a');

      },
    triggerImage: function(event){
          console.log('event.target.files[0]');
          console.log(event.target.files[0]);
          let file = event.target.files[0];
          let form = new FormData();
          form.append('_token','{{csrf_token()}}');
          form.append('file',file);
          form.append('id',this.recruiterId);
          form.append('admin_id',this.adminID);
          form.append('recruiter_name',this.recruiter_name);
          form.append('recruiter_id',this.recruiterId);
          let url = '{{ route('postUserMessage') }}';
          this.$http.post(url,form).then((response) => {
              console.log(response);
              this.getMessages();
          }).catch((error) => {
              console.log(error);
          });
    },
      submitMessage: function(){
            if(this.enterMessage == '') {
                alert('please enter message first');
            } else {
              let data = {
                  '_token' : '{{csrf_token()}}',
                  'message':this.enterMessage,
                  'recruiter_id':this.recruiterId,
                  'recruiter_name':this.recruiter_name,
                  'admin_id':this.adminID,
              };
              let url = '{{ route('postUserMessage') }}';
              this.$http.post(url,data).then((response) => {
                  console.log('response');
                  console.log(response);
                  if(response.data.status == true) {
                    this.enterMessage = '';
                    this.getMessages();
                  }
              }).catch((error) => {
                  console.log('error');
                  console.log(error);
              });
          }
      },
      clickToRefresh: function(){
          this.spinLoader = true;
          this.getMessages();
      },
      getMessages: function(){
        let url = '/post-get-user-message/'+this.recruiterId+'/'+this.recruiter_name;
        this.$http.get(url).then((response) => {
          console.log('response');
          console.log(response.data.data);
          this.allMessages = response.data.data;
          this.spinLoader = false;
        }).catch((error) => {
          console.log('error');
          console.log(error);
        });

      },
  },
  mounted(){
  	console.log('admin vuejs initial');
    this.getMessages();
    // let that = this;
    // console.log(this.recruiter_image);
    //   setInterval(function(){
    //       console.log('setInterval start');
    //       that.getMessages();
    //   }, 3000);
  }
})
</script>
@endpush
