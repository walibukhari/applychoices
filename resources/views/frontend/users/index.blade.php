@extends('layouts.default')

@push('css')
<style type="text/css">
    .hoverEffec{
        margin: 24px 0 0 0;
    padding: 10px 35px;
    font-family: 'Eina03-SemiBold';
    font-size: 20px;
    text-align: center;
    text-decoration: none;
    color: #DAA520;
    background: #fff;
    border-radius: 6px;
    display: inline-block;
    letter-spacing: 1px;
    }
    .hoverEffec:hover{
        color: #fff;
        background: #DAA520 ;
    }
</style>
@endpush

@section('content')

<section class="Registerform-Banner">
    <img src="/assets/image/register-banner.png" width="1920" height="550" alt=""/>
</section>
@if(session()->has('success_me'))
<div class="row" style="width: 100%;">
                    <div class="alert alert-success" style="margin-bottom: 0px;width: 100%;">
                        {{session()->get('success_me')}}
                    </div>
</div>
@endif
<section class="Section-Registerform">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="Registerform-Heading">
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s">User Profile</h1>
                </div>
            </div>
       <form action="{{route('user.profile.update')}}" method="POST">
           @csrf
           <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.1s">
                            <label>Name</label>
                            <input type="text" name="name" id="name" value="{{\Auth::user()->name}}" placeholder="Enter Your Name" required="name">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.2s">
                            <label>Email</label>
                            <input type="email" name="email" value="{{\Auth::user()->email}}" placeholder="Enter Email" required="email">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.7s">
                            <label>Password</label>
                            <input type="password" name="password" autocomplete="off" id="password" placeholder="Enter Your Password">
                        </div>
                    </div>
                </div>
            <div class="pull-right">
                <button type="submit" class="btn hoverEffec">Update</button>
                @if(\Auth::user()->register_as == 1)
                <a href="{{route('chatName')}}" class="btn hoverEffec">Chat</a>
                @endif
            </div>
        </form>
        </div>
    </div>
</section>

@endsection

@push('js')
<script>

    $(document).ready(function(){
        $("#AboutUs-slider").owlCarousel({
            items:1,
            itemsDesktop:[1000,2],
            itemsDesktopSmall:[979,2],
            itemsTablet:[768,1],
            pagination:true,
            autoPlay:true
        });
    });

</script>
@endpush
