@extends('layouts.default')

@push('css')
<style>

</style>
@endpush


@section('content')
<section class="Recruiters-Banner-Bg">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-12">
                <div class="School-BannerHead">
                    <h1 class="wow fadeInUp" data-wow-delay="0.2s">Help Your Students</h1>
                    <h2 class="wow fadeInUp" data-wow-delay="0.4s">Study Abroad</h2>
                    <p class="wow fadeInUp" data-wow-delay="0.6s">Give your students more options globally with<br /> access to over 1,200 schools. Simplify the<br /> application, acceptance, and arrival process<br /> through a single online platform.</p>
                    <a class="wow fadeInUp" data-wow-delay="0.8s" href="{{route('agentDashboard')}}">Work With Us</a>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="Section-Benefit">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="BenefitsHead">
                    <h1 class="wow fadeInUp" data-wow-delay="0.2s">Our Benefits</h1>
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="BenefitsBox wow fadeInUp" data-wow-delay="0.2s">

                            <div class="Benefits-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>

                            <div class="Benefits-content">
                                <h3 class="Benefitstitle">Access 1,200+ Schools</h3>
                                <p class="Benefitsdescription">ApplyChoices provides a platform to access more than 30,000 programs across all areas of study in reputed schools.</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="BenefitsBox wow fadeInUp" data-wow-delay="0.4s">

                            <div class="Benefits-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>

                            <div class="Benefits-content">
                                <h3 class="Benefitstitle">Find Programs Faster</h3>
                                <p class="Benefitsdescription">Students easily shortlist the program that best matches their interest using an efficient search system of ApplyChoices web.</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="BenefitsBox wow fadeInUp" data-wow-delay="0.6s">

                            <div class="Benefits-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>

                            <div class="Benefits-content">
                                <h3 class="Benefitstitle">One Easy Application</h3>
                                <p class="Benefitsdescription">ApplyChoices allows new users to create a profile once and then apply to any program using a previous profile with some modifications and additional documents attachment if required.</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="BenefitsBox wow fadeInUp" data-wow-delay="0.8s">

                            <div class="Benefits-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>

                            <div class="Benefits-content">
                                <h3 class="Benefitstitle">A+ Customer Experience</h3>
                                <p class="Benefitsdescription">ApplyChoices offer a devoted group that upholds dedicated students and paved their way from application submission to final visa issuance and arrival at school.</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="BenefitsBox wow fadeInUp" data-wow-delay="0.10s">

                            <div class="Benefits-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>

                            <div class="Benefits-content">
                                <h3 class="Benefitstitle">A Central Platform</h3>
                                <p class="Benefitsdescription">ApplyChoices presents a central platform to deal with students, their applications, and communication with the school in a single spot.</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="BenefitsBox wow fadeInUp" data-wow-delay="0.12s">

                            <div class="Benefits-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>

                            <div class="Benefits-content">
                                <h3 class="Benefitstitle">Perks And Rewards</h3>
                                <p class="Benefitsdescription">ApplyChoices allows its partners and recruiters to win liberal commissions, rewards, and discounts to help their development.</p>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="ApplyChoices-Bg">
    <div class="container">
        <div class="row">

            <div class="col-xl-6 col-md-6 col-12">
                <div class="ApplyAbroad-Head wow fadeInUp" data-wow-delay="0.2s">
                    <h5>How ApplyChoices Works</h5>
                    <h6>ApplyChoices is a global study choice platform. Our artificial intelligence (AI) controlled system serves to discover schools that meet your student’s interests, apply to numerous programs that coordinate their capabilities, and gain effective commissions.</h6>
                    <a href="#">Learn Now</a>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12">
                <div class="ApplyAbroad-Head wow fadeInUp" data-wow-delay="0.4s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

        </div>
    </div>
</section>

<section class="Section-OurImpact">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="OurImpact-Head">
                    <h1 class="wow fadeInUp" data-wow-delay="0.2s">Our Impact</h1>
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="counter wow fadeInUp" data-wow-delay="0.2s">
                            <div class="counter-content">

                                <div class="counter-icon"></div>
                                <span class="counter-value">100000</span>

                            </div>
                            <h3>Students Helped</h3>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="counter wow fadeInUp" data-wow-delay="0.4s">
                            <div class="counter-content">

                                <div class="counter-icon"></div>
                                <span class="counter-value">95%</span>

                            </div>
                            <h3>Acceptance Rate</h3>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="counter wow fadeInUp" data-wow-delay="0.6s">
                            <div class="counter-content">

                                <div class="counter-icon"></div>
                                <span class="counter-value">1200</span>

                            </div>
                            <h3>School Partnerships</h3>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="SectionRec-Partners">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <h4 class="wow fadeInUp" data-wow-delay="0.2s">Join 4,000+ Global ApplyChoices<br /> Recruitment Partners</h4>
                <a class="wow fadeInUp" data-wow-delay="0.4s" href="{{route('agentDashboard')}}">Partner With Us</a>
            </div>

        </div>
    </div>
</section>

<section class="Section-AboutSlider">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div id="AboutUs-slider" class="owl-carousel wow fadeInUp" data-wow-delay="0.2s">

                    <div class="testimonial">

                        <p class="description">
                            I began searching on Google for different universities in abroad and I was diverted to various sites, one of which is ApplyChoices.com, which ended up being the most helpful. ApplyChoices helped by giving me a wide scope of point by point choices of schools to look over. Finally, I choose the one school in Canada that best fit my interest. That’s why I am thankful to ApplyChoices for their full support and professional guidance.
                        </p>

                        <div class="testimonial-content">
                            <h3 class="title">- Bahri S., International Student at Conestoga College</h3>
                        </div>


                    </div>

                    <div class="testimonial">

                        <p class="description">
                            I began searching on Google for different universities in abroad and I was diverted to various sites, one of which is ApplyChoices.com, which ended up being the most helpful. ApplyChoices helped by giving me a wide scope of point by point choices of schools to look over. Finally, I choose the one school in Canada that best fit my interest. That’s why I am thankful to ApplyChoices for their full support and professional guidance.
                        </p>

                        <div class="testimonial-content">
                            <h3 class="title">- Bahri S., International Student at Conestoga College</h3>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="ApplyChoices-Bg">
    <div class="container">
        <div class="row">

            <div class="col-xl-6 col-md-6 col-12 d-none d-sm-block">
                <div class="ApplyAbroad-Head wow fadeInUp" data-wow-delay="0.2s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12">
                <div class="ApplyAbroad-Head wow fadeInUp" data-wow-delay="0.4s">
                    <h5>Work with ApplyChoices</h5>
                    <h6>Recruitment accomplices around the globe are improving admittance to education through ApplyChoices. Our outcomes-based channel help recruiters to enroll potential students in universities worldwide. See what accomplices state about working with us.</h6>
                </div>
            </div>

            <div class="col-xl-6 col-md-6 col-12 d-xl-none d-block d-sm-none">
                <div class="ApplyAbroad-Head wow fadeInUp" data-wow-delay="0.2s">
                    <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt="">
                </div>
            </div>

        </div>
    </div>
</section>

<div class="Section-LogoBranding">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="LogoBranding-Head wow fadeInUp" data-wow-delay="0.2s">
                    <h4>Access to More <span>Opportunities</span></h4>
                    <p>Here you go with the other eye-opening opportunities that will help students to take a wider look and to reflect the perfection of their decision.</p>
                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div id="Branding-Slider" class="owl-carousel wow fadeInUp" data-wow-delay="0.3s">

                    <div class="LogoBrand-Image">
                        <a href="#" target="_blank"><img src="{{asset('assets/image/branding-01.png')}}" width="155" height="75" alt=""/></a>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-02.png')}}" width="247" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-03.png')}}" width="312" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-04.png')}}" width="340" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-05.png')}}" width="257" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-06.png')}}" width="1147" height="300" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-01.png')}}" width="155" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-02.png')}}" width="247" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-03.png')}}" width="312" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-04.png')}}" width="340" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-05.png')}}" width="257" height="75" alt=""/>
                    </div>

                    <div class="LogoBrand-Image">
                        <img src="{{asset('assets/image/branding-06.png')}}" width="1147" height="300" alt=""/>
                    </div>

                </div>
            </div>

            <div class="col-xl-12 col-md-12 col-12">
                <div class="SliderBtn">
                    <a href="#">Explore Institutions</a>
                </div>
            </div>

        </div>
    </div>
</div>

<section class="SectionEvents">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <h4 class="wow fadeInUp" data-wow-delay="0.2s">Events and Webinars</h4>
                <p class="wow fadeInUp" data-wow-delay="0.4s">Meet us in person at events around the world or connect with <br />us virtually at one of our webinars.</p>
                <a class="wow fadeInUp" data-wow-delay="0.6s" href="#">Learn More</a>
            </div>

        </div>
    </div>
</section>

<section class="Section-EducateWord">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="EducateWord-Head">
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s">Help Us Educate The World</h1>
                    <a class="wow fadeInUp" data-wow-delay="0.6s" href="{{route('agentDashboard')}}">Work With Us</a>
                </div>
            </div>

        </div>
    </div>
</section>


@endsection


@push('js')

<script>
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 50,
        mobile: false
    });
    wow.init();
</script>

<script>

    $(document).ready(function(){
        $('.counter-value').each(function(){
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            },{
                duration: 3500,
                easing: 'swing',
                step: function (now){
                    $(this).text(Math.ceil(now));
                }
            });
        });
    });

</script>

<script>

    $(document).ready(function(){
        $("#AboutUs-slider").owlCarousel({
            items:1,
            itemsDesktop:[1000,2],
            itemsDesktopSmall:[979,2],
            itemsTablet:[768,1],
            pagination:true,
            autoPlay:true
        });
    });

</script>

<script>

    $(document).ready(function(){
        $("#Branding-Slider").owlCarousel({
            items:6,
            itemsDesktop:[1000,6],
            itemsDesktopSmall:[979,4],
            itemsTablet:[768,3],
            pagination:false,
            navigation:true,
            transitionStyle:"backSlide",
            navigationText:["<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"],
            autoPlay:true
        });
    });

</script>

@endpush
