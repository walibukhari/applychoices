@extends('layouts.default')

@push('css')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/rowReorder.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/globel.css')}}" rel="stylesheet" type="text/css">

    <style>
         .HeaderTop-Nav .navbar {
    padding: 0;
    float: right;
    margin-bottom: -4px;
}

         [v-cloak] {
             display: none;
         }
         .AgentsProfile {
             margin: 0 auto;
             padding: 40px 0 50px 0px;
             text-align: center;
             display: flex;
             flex-direction: column;
             align-items: center;
         }
    </style>
@endpush

@section('content')
<section class="Agents-BodyContent" id="recruiter" v-cloak>

    <div class="container-fluid">
        <div class="row">

            <div class="col-xl-3 col-md-3 col-12">
                <div class="AgentsLeft wow fadeInUp" data-wow-delay="0.2s">

                    <div class="AgentsProfile">
                        <img id="output" :src="recruiter_image" onerror="this.src='{{asset('assets/image/profile-image.png')}}'" width="200" height="200" alt=""/>
                        <button type="button" @click="uploadImage()" class="btn btn-warning btn-sm" style="
                        width: -webkit-fill-available;
                         width: 42%;">Update Image
                            <input type="file" name="file" @change="triggerImage($event)" id=uploadImage style="display: none;" />
                        </button>

                        <h1 style="cursor: pointer;" @mouseleave ="hoverEditLeave()" @mouseover ="hoverEdit()"
                        >{{\Auth::user()->name}}
                            <i class="fa fa-pencil-square-o" @click="EditModal()" style="color: rgb(218, 165, 32);" v-if="hoverTrue" aria-hidden ="true"></i>
                        </h1>

                        <h2 style="cursor: pointer;" @mouseleave="hoverEditLeave()" @mouseover="hoverEdit()">{{\Auth::user()->phone_number}}
                            <i class="fa fa-pencil-square-o" @click="openEditModal()" style="color: rgb(218, 165, 32);" v-if="hoverTrue" aria-hidden="true"></i>
                        </h2>
                        <!--  <h3><i class="fa fa-birthday-cake"></i> 19 April, 1995, 0 Years</h3>
                         <h3><i class="fa fa-map-marker"></i> Sialkot</h3> -->
                    </div>

                    <div class="clearfix"></div>

                    <div class="AgentsListing-Menu">
                        <ul>
                            <li><a href="{{route('agentDashboard')}}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                            <li><a href="{{route('agentDashboard')}}"><i class="fa fa-clock-o"></i> Appoinment </a></li>
                            <li><a href="{{route('user.chats')}}"><i class="fa fa-clock-o"></i> Chat </a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-xl-9 col-md-9 col-12">
                <div class="AgentsAccordion wow fadeInUp" data-wow-delay="0.4s">
                    <div class="tab" role="tabpanel">

                        <ul class="nav nav-tabs" role="tablist">

                            <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Pending Leads</a></li>

                            <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Approved Leads</a></li>

                        </ul>

                        <div class="tab-content tabs">

                            <div role="tabpanel" class="tab-pane in active" id="Section1">
                                <div class="Profile-Table">

                                    <table id="example" class="table table-bordered display responsive nowrap" style="width: 100%;">

                                        <thead>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Phone Number</th>
                                            <th>City</th>
                                            <th>Email Address</th>
                                            <th>Address</th>
                                            <th>Country</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($recruiters as $recruiter)
                                               <tr>
                                                 <td>{{$recruiter->user_name}}</td>
                                                 <td>{{$recruiter->phone_number}}</td>
                                                 <td>{{$recruiter->city}}</td>
                                                 <td>{{$recruiter->email_address}}</td>
                                                 <td>{{$recruiter->address}}</td>
                                                 <td>{{$recruiter->country}}</td>
                                              </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="Section2">
                                <div class="AgentsForm-Container">
                                    <form action="{{route('recruiter.submit')}}" method="GET">
                        @csrf
                                    <div class="row">

                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="Agents-ContactForm">
                                                <label>User Name</label>
                                                <input type="text" id="user_name" name="user_name" placeholder="Enter Your Name" required="user_name">
                                            </div>
                                        </div>

                                          <div class="col-xl-6 col-md-6 col-12">
                                            <div class="Agents-ContactForm">
                                                <label>Phone Number</label>
                                                <input type="text" id="phone_number" name="phone_number" placeholder="Enter Your Name" required="phone_number">
                                            </div>
                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="Agents-ContactForm">
                                                <label>Email ID</label>
                                                <input type="text" id="email_address" name="email_address" placeholder="Enter Your Email" required="email_address">
                                            </div>
                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="Agents-ContactForm">
                                                <label>City</label>
                                                <input type="text" id="city" name="city" placeholder="Enter Your City" required="city">
                                            </div>
                                        </div>

                                         <div class="col-xl-6 col-md-6 col-12">
                                            <div class="Agents-ContactForm">
                                                <label>Country</label>
                                                <input type="text" id="country" name="country" placeholder="Enter Your Country" required="country">
                                            </div>
                                        </div>

                                        <div class="col-xl-12 col-md-12 col-12">
                                            <div class="Agents-StatusList">


                                            </div>
                                        </div>

                                        <div class="col-xl-12 col-md-12 col-12">
                                            <div class="Agents-ContactForm">
                                                <label>Address</label>
                                                <input type="text" id="address" name="address" placeholder="Enter Your Address" required="address">
                                            </div>
                                        </div>

                                        <div class="col-xl-12 col-md-12 col-12">
                                            <div class="Agents-ContactForm">
                                                <button type="submit">Save Changes</button>
                                            </div>
                                        </div>

                                    </div>
                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal" id=modalOpem tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('phoneNumber')}}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label>Phone Number:</label>
                            <input type="number" id="phone_number" value="{{\Auth::user()->phone_number}}" class="form-control" name="phone_number" placeholder="Enter Your Name" required="phone_number">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="modal" id=openmodel tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('postName')}}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" id="name" value="{{\Auth::user()->name}}" class="form-control" name="name" placeholder="Enter Your Name" required="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.rowReorder.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.responsive.min.js')}}"></script>
<script>

    $(document).ready(function() {

        $('#example').dataTable({
            searching: true,
            "sDom": '<"row view-filter"<"col-sm-12"<"pull-right"l><"pull-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
        });


        $('#example-1').dataTable({
            searching: false,
        });

    });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js" integrity="sha512-Izh34nqeeR7/nwthfeE0SI3c8uhFSnqxV0sI9TvTcXiFJkMd6fB644O64BRq2P/LA/+7eRvCw4GmLsXksyTHBg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js" integrity="sha512-v8ng/uGxkge3d1IJuEo6dJP8JViyvms0cly9pnbfRxT6/31c3dRWxIiwGnMSWwZjHKOuY3EVmijs7k1jz/9bLA==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js" integrity="sha512-wGKmIfDWUJSUvxUfUayQPJj7ADCD60La3up0VCbq+MTFcOUQ2hlH2McnYFafHgLTsOrGwOdiHKX4p1v0BerCyQ==" crossorigin="anonymous"></script>

<script type="text/javascript">
    var app = new Vue({
        el: '#recruiter',
        data: {
            message: 'Hello Vue!',
            recruiter : [],
            username:'',
            enterMessage:'',
            recruiter_id:'',
            allMessages:[],
            recruiterId:'{{ \Auth::user()->id }}',
            recruiter_name:'{{ \Auth::user()->name }}',
            recruiter_image:'{{ \Auth::user()->image }}',
            adminID:1,
            spinLoader:false,
            hoverTrue:false

        },
        methods:{
            downloadFile: function (id){
                window.location.href='/download/'+id;
            },
            getDate: function (date) {
                return moment(date).format('MM/DD/YYYY , h:mm:ss a');

            },
            uploadFile: function (){
                $('#getFile').click();
            },
            replaceName: function (name){
                let fileName = name.split('chat/');
                return fileName[1];
            },
            onEnter: function () {
                this.submitMessage();
            },
            EditModal: function(){
                $('#openmodel').modal('show');
            },
            openEditModal: function(){
                $('#modalOpem').modal('show');
            },
            hoverEdit: function(){
                this.hoverTrue = true;
            },
            hoverEditLeave: function(){
                this.hoverTrue = false;
            },
            uploadImage: function(){
                $('#uploadImage').click();
            },
            triggerFile: function(event){
                console.log('event.target.files[0]');
                console.log(event.target.files[0]);
                let file = event.target.files[0];
                let form = new FormData();
                form.append('_token','{{csrf_token()}}');
                form.append('file',file);
                form.append('id',this.recruiterId);
                form.append('admin_id',this.adminID);
                form.append('recruiter_name',this.recruiter_name);
                form.append('recruiter_id',this.recruiterId);
                let url = '{{ route('postUserMessage') }}';
                this.$http.post(url,form).then((response) => {
                    console.log(response);
                    this.getMessages();
                }).catch((error) => {
                    console.log(error);
                });
            },
            triggerImage: function(event){
                var image = document.getElementById('output');
                image.src = URL.createObjectURL(event.target.files[0]);
                console.log('event.target.files[0]');
                console.log(event.target.files[0]);
                let file = event.target.files[0];
                let form = new FormData();
                form.append('_token','{{csrf_token()}}');
                form.append('file',file);
                form.append('id',this.recruiterId);
                let url = '{{ route('postUploadImage') }}';
                this.$http.post(url,form).then((response) => {
                    console.log(response);
                    if(response.data.data.status == true) {
                    }
                }).catch((error) => {
                    console.log(error);
                });
            },
            submitMessage: function(){
                if(this.enterMessage == '') {
                    alert('please enter message first');
                } else {
                    let data = {
                        '_token' : '{{csrf_token()}}',
                        'message':this.enterMessage,
                        'recruiter_id':this.recruiterId,
                        'recruiter_name':this.recruiter_name,
                        'admin_id':this.adminID,
                    };
                    let url = '{{ route('postUserMessage') }}';
                    this.$http.post(url,data).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            this.enterMessage = '';
                            this.getMessages();
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                }
            },
            clickToRefresh: function(){
                this.spinLoader = true;
                this.getMessages();
            },
            getMessages: function(){
                let url = '/post-get-user-message/'+this.recruiterId+'/'+this.recruiter_name;
                this.$http.get(url).then((response) => {
                    console.log('response');
                    console.log(response.data.data);
                    this.allMessages = response.data.data;
                    this.spinLoader = false;
                }).catch((error) => {
                    console.log('error');
                    console.log(error);
                });

            },
        },
        mounted(){
            console.log('admin vuejs initial');
            this.getMessages();
            console.log(this.recruiter_image);
        }
    })
</script>

@endpush
