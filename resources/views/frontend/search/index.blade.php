@extends('layouts.default')

@push('css')
    <link href="{{asset('assets/css/select-chosen.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/globel.css')}}" rel="stylesheet" type="text/css">
    <style>
        [v-cloak]{
            display: none;
        }
        .ui-menu-item .ui-menu-item-wrapper.ui-state-active {
            background: transparent !important;
            font-weight: normal !important;
            border: 0px !important;
            padding-left: 6px !important;
        }
        .ui-menu-item:hover{
            color: #555 !important;
            background: #DAA520 !important;
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .ui-menu .ui-menu-item-wrapper{
            padding-left: 6px !important;
            text-underline: none !important;
            margin-bottom: 6px !important;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 64px;
            height: 64px;
            margin: 8px;
            border: 8px solid #555;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #555 transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .ProgramsContainer .ProgramsLeft a {
            margin: 10px 0 0 0;
            padding: 5px 30px;
            text-align: center;
            text-decoration: none;
            text-transform: uppercase;
            background: #80011f;
            border-radius: 4px;
            border: none;
            outline: none;
            font-family: 'Eina03-SemiBold';
            font-size: 15px;
            color: #fff;
            letter-spacing: 1.5px;
            float: right;
            -webkit-transition: all ease-in 0.3s;
            -moz-transition: all ease-in 0.3s;
            -o-transition: all ease-in 0.3s;
            transition: all ease-in 0.3s;
        }

        .select2-container--default .select2-selection--single .select2-selection__clear{
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 26px;
            position: absolute;
            top: 1px;
            right: 1px;
            width: 20px;
            display: none;
        }
        .select2-container:focus{
            border: 0px !important;
        }
        .select2-container--default .select2-selection--single {
            position: relative;
            overflow: hidden;
            margin: 0;
            padding: 6px 15px;
            width: 100%;
            height: auto !important;
            background-color: #fff;
            cursor: text;
            border: 1px solid #adb5bd;
            font-family: 'Eina03-Regular';
            font-size: 15px;
            -webkit-transition: all ease-in 0.3s;
            -moz-transition: all ease-in 0.3s;
            -o-transition: all ease-in 0.3s;
            transition: all ease-in 0.3s;
            box-shadow: none;
            background-image: none;
            border-radius: 6px;
        }
        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
        }
    </style>
    <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        .Section-SearchFilter {
            margin: 0;
            padding: 50px 0;
            background: #f8f9fa;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
@section('content')
<section class="Section-SearchFilter" id="searchFilters">
    <div class="loading" v-if="fullLoader">Loading&#8230;</div>
    <div v-if="!fullLoader" class="container" style="max-width: 85%;">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="MainSearch wow fadeInUp" data-wow-delay="0.2s">
                    <input id="search" name="" @keyup="enterClick($event)" autocomplete="off" placeholder="What would you like to study?">
                    <input id="search2" @keyup="enterClick($event)" class="activeborder" autocomplete="off" type="text" name="" placeholder="Where? e.g. school name or location">
                    <button type="button" @click="searchData()"><i class="fa fa-search"></i></button>
                    <div id="noResultsFound" style="display:none;color: red;">No Result Found</div>
                </div>
            </div>

            <div class="col-xl-5 col-md-5 col-12">
                <div class="AccordionTab-Left wow fadeInUp" data-wow-delay="0.4s">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Eligibility
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse in show"
                                 role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">

                                    <div class="SelectBox-Multiple">
                                        <label>Do you have a valid Study Permit / Visa?</label>
                                        <select class="multipleChosen" id="study_permit" multiple="true" v-model="study_permit">
                                            <option value="1">I don't have this</option>
                                            <option value="2">USA F1 Visa</option>
                                            <option value="3">Canadian Study Visa Or Visitor Visa</option>
                                            <option value="4">UK Student Visa (Tier 4) Or Short Term Study Visa</option>
                                        </select>
                                    </div>

                                    <div class="LabelSelect">
                                        <label>Nationality</label>
                                        <select class="js-example-basic-multiple10" name="select" id="nationality" v-model="nationality">
                                            <option value="4" selected>Select...</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="LabelSelect">
                                        <label>Education Country</label>
                                        <select class="js-example-basic-multiple0" name="select" id="country" v-model="edu_country">
                                            <option value="4" selected>Select...</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="LabelSelect">
                                        <label>Education Level</label>
                                        <div class="select">
                                            <i class="fa fa-angle-down"></i>
                                            <select name="select" id="edu_level" v-model="edu_level">
                                                <option value="4" selected>Select...</option>
                                                <option value="Grade 10">Grade 1</option>
                                                <option value="Grade 2">Grade 2</option>
                                                <option value="Grade 9">Grade 9</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="LabelSelect">
                                        <label>Grading Scheme</label>
                                        <div class="select">
                                            <i class="fa fa-angle-down"></i>
                                            <select name="select" id="grade_scheme" v-model="grade_scheme">
                                                <option :value="4" selected>Select...</option>
                                                <option :value="1">Secondary Level - Scale:0-100</option>
                                                <option :value="3">Other</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="LabelSelect">
                                        <label>English Exam Type</label>
                                        <div class="select">
                                            <i class="fa fa-angle-down"></i>
                                            <select name="select" id="exam_type" v-model="exam_type">
                                                <option :value="4" selected>Select...</option>
                                                <option :value="1">I don't have this</option>
                                                <option :value="2">TOEFL</option>
                                                <option :value="3">IELTS</option>
                                                <option :value="3">Duolingo English Test</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="FilterBtn-Filter">
                                        <label class="PartnerCheckbox filterHead-One">
                                            Only Show Direct Admissions
                                            <input type="checkbox" checked="checked">
                                            <span class="Partnercheckmark filterHead-two"></span>
                                        </label>
                                        <button type="button" @click="applyEligiblityFilter()">Apply Filters</button>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        School Filters
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseTwo" class="panel-collapse collapse in show"
                                 role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
{{--                                    <div class="SelectBox-Multiple">--}}
{{--                                        <label>Countries</label>--}}
{{--                                        <select class="multipleChosen" id="selectCountry" onchange="selectCountry(event)" multiple="true">--}}
{{--                                            <option value="216">USA</option>--}}
{{--                                            <option value="38">Canada</option>--}}
{{--                                            <option value="12">United Kingdom</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
                                    <div class="SelectBox-Multiple">
                                        <label>Countries</label>
                                        <select class="js-example-basic-multiple" id="countries" name="countries[]" multiple="multiple">
                                            <option value="216">USA</option>
                                            <option value="38">Canada</option>
                                            <option value="215">United Kingdom</option>
                                        </select>
                                    </div>

                                    <div class="FilterBtn-Filter">
                                        <label class="PartnerCheckbox filterHead-One mb-4">
                                            Only Show Direct Admissions
                                            <input type="checkbox" checked="checked">
                                            <span class="Partnercheckmark filterHead-two"></span>
                                        </label>
                                    </div>

                                    <div class="SelectBox-Multiple">
                                        <label>Provinces / States</label>
                                        <div class="SelectBox-Multiple">
                                            <select class="js-example-basic-multiple2" id="states" name="states[]" multiple="multiple">
                                                @foreach($states as $st)
                                                    <option value="{{$st->id}}">{{$st->state}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="SchoolTypes-List">
                                        <h3>School Types</h3>
                                        <ul>

                                            <li>
                                                <label class="PartnerCheckbox filterHead-One">University
                                                    <input type="checkbox" id="university" @change="selectCheckbox('a')">
                                                    <span class="Partnercheckmark filterHead-two"></span>
                                                </label>
                                            </li>

                                            <li>
                                                <label class="PartnerCheckbox filterHead-One">College
                                                    <input type="checkbox" id="college"  @change="selectCheckbox('b')">
                                                    <span class="Partnercheckmark filterHead-two"></span>
                                                </label>
                                            </li>

                                            <li>
                                                <label class="PartnerCheckbox filterHead-One">English Institute
                                                    <input type="checkbox" id="eng_institute"  @change="selectCheckbox('c')">
                                                    <span class="Partnercheckmark filterHead-two"></span>
                                                </label>
                                            </li>

                                            <li>
                                                <label class="PartnerCheckbox filterHead-One">High School
                                                    <input type="checkbox" id="high_school" @if($filter == 'schools') checked @endif  @change="selectCheckbox('d')">
                                                    <span class="Partnercheckmark filterHead-two"></span>
                                                </label>
                                            </li>

                                        </ul>
                                    </div>

                                    <div class="SelectBox-Multiple">
                                        <label>Schools</label>
                                        <select class="multipleChosen" id="schools" multiple="true">
                                            @foreach($school as $sc)
                                                <option value="{{$sc->id}}">{{$sc->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="ClearFilters-Btn">
                                        <button type="submit" @click="applySchoolFilter()">Apply Filters</button>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Program Filters
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseThree" class="panel-collapse collapse in show"
                                 role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">

                                    <div class="SelectBox-Multiple">
                                        <label>Program Levels</label>
                                        <select class="multipleChosen" id="programLevel" multiple="true">
                                            @foreach($program_level as $program)
                                                <option value="{{$program->id}}" @if($program->program_type == $filter) selected @endif >{{$program->program_level}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="SelectBox-Multiple">
                                        <label>Intakes</label>
                                        <select class="multipleChosen" multiple="true">
                                            <option value="17">September 2020</option>
                                        </select>
                                    </div>

                                    <div class="SelectBox-Multiple">
                                        <label>Post-Secondary Discipline</label>
                                        <select class="multipleChosen" multiple="true">
                                            <option value="18">Sciences</option>
                                        </select>
                                    </div>

                                    <div class="SelectBox-Multiple">
                                        <label>Post-Secondary Sub-Categories</label>
                                        <select class="multipleChosen" multiple="true">
                                            <option value="19">Biochemistry</option>
                                        </select>
                                    </div>

                                    <h6 class="ListingParagraph">All amounts are listed in the currency charged by the school. For best results, please specify a country of the school.</h6>

                                    <div class="TuitionFee-Container">

                                        <div class="TuitionLeft">
                                            <h4>Tuition Fee</h4>
                                        </div>

                                        <div class="TuitionRight">
                                            <label class="PartnerCheckbox filterHead-One">
                                                Include living costs
                                                <input type="checkbox" checked="checked">
                                                <span class="Partnercheckmark filterHead-two"></span>
                                            </label>
                                        </div>

                                        <div class="range-slider range-slider1">
                                            <input type="range" value="1" min="0" max="100k" range="true">
                                            <span class="range-value" id="rangerSlier1">1k</span>
                                        </div>

                                        <div class="range-slider">
                                            <h4 class="ApplicatinFees">Application Fee</h4>
                                            <input type="range" value="1" min="0" max="500" range="true">
                                            <span class="range-value" id="rangerSlier2">1</span>
                                        </div>

                                        <div class="ClearFilters-Btn">
                                            <button class="active" type="reset" @click="clearFilter()">Clear Filters</button>
                                            <button type="button" @click="applyProgramFilters()">Apply Filters</button>
                                        </div>

                                    </div>


                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <div class="col-xl-7 col-md-7 col-12">
                <div class="tab wow fadeInUp" role="tabpanel" data-wow-delay="0.6s" v-cloak>

                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Programs (@{{ programLength }})</a></li>

                        <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">School (@{{ schoolLength }})</a></li>

                    </ul>

                    <div class="tab-content tabs">

                        <div role="tabpanel" class="tab-pane in active" id="Section1">
                            <div class="ProgramsContainer" v-if="programLength == 0">
                                No Results Found
                            </div>
                            <div class="ProgramsContainer" style="display: flex;justify-content: center;" v-if="loader">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                            <div class="ProgramsContainer" v-if="!loader" v-for="results in programData">
                                <div class="ProgramsLeft" v-if="results">

                                    <img :src="getSchoolLogo(results.school_id)" width="200" height="200" alt=""/>
                                    <h1>@{{ getSchoolName(results.school_id) }}</h1>
                                    <br>
                                    <h2>
                                        <img src="{{asset('assets/image/flag-2.png')}}" width="48" height="48" alt=""/>
                                        @{{ getSchoolCountry(results.school_id) }}
                                    </h2>
                                    <p>
                                        @{{ results.program_name }}
                                    </p>
                                    <div class="clearfix"></div>

                                    <h3>@{{ results.description }}</h3>
                                    <ul>
                                        <li><h4><span>TUITION FEE</span> $ @{{ results.tuition_fee }} USD</h4></li>
                                        <li><h4><span>APPLICATION FEE</span> $ @{{ results.application_fee }} USD</h4></li>
                                    </ul>

                                    <a :href="'/application-form/'+results.id" target="__blank">Apply</a>

                                </div>

                            </div>

                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="Section2">
                            <div class="SchoolContainer" v-if="schoolLength == 0">
                                No Results Found
                            </div>
                            <div class="SchoolContainer" style="display: flex;justify-content: center;" v-if="loader">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                            <div v-if="!loader" v-for="school in  schoolData">
                                <div v-if="school" class="SchoolContainer" >
                                        <div class="SchoolContent-Left">
                                            <img :src="school.logo_link" width="200" height="200" alt=""/>
                                        </div>

                                        <div class="SchoolContent-Right">
                                            <a href="#">@{{ school.name }}</a>
                                            <h6>@{{ school.country }} , @{{ school.state }}</h6>
                                        </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
@endsection
@push('js')
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .select2-container--default .select2-search--inline .select2-search__field{
        margin: 1px 0;
        padding: 0;
        height: 25px;
        outline: 0;
        border: 0!important;
        background: transparent!important;
        box-shadow: none;
        color: #999;
        font-size: 100%;
        font-family: sans-serif;
        line-height: normal;
        border-radius: 0;
    }
    .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable{
        background-color: #DAA520;
        color: white;
    }
    .select2-container--default .select2-selection--multiple{
        padding: 6px 15px;
        font-family: 'Eina03-Regular';
        font-size: 15px;
        transition: all ease-in 0.3s;
        box-shadow: none;
        background-image: none;
        border-radius: 6px;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        position: relative;
        padding: 5px 24px 8px 8px;
        border: 1px solid #80011f;
        max-width: 100%;
        border-radius: 3px;
        background-color: #80011f;
        background-image: -webkit-gradient(linear,50% 0,50% 100%,color-stop(20%,#f4f4f4),color-stop(50%,#f0f0f0),color-stop(52%,#e8e8e8),color-stop(100%,#eee));
        background-image: -webkit-linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
        background-image: -moz-linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
        background-image: -o-linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
        background-image: none;
        background-size: 100% 19px;
        background-repeat: repeat-x;
        background-clip: padding-box;
        box-shadow: none;
        color: #fff;
        line-height: 20px;
        cursor: default;
        font-family: 'Eina03-Regular';
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove:focus{
        background: transparent !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
        border: 0px !important;
        color: #fff !important;
    }
    .ui-widget.ui-widget-content {
        border: 1px solid #c5c5c5;
        width: 400px !important;
        padding-top: 12px;
    }
    .ui-menu-item .ui-menu-item-wrapper.ui-state-active {
        font-weight: bold !important;
        color: #ffffff !important;
    }
</style>
<script>

    $(document).ready(function() {
        $(".js-example-basic-multiple").select2({
            placeholder: "Select...",
            allowClear: true,
        });
        $(".js-example-basic-multiple2").select2({
            placeholder: "Select...",
            allowClear: true,
        });
        $(".js-example-basic-multiple0").select2({
            placeholder: "Select...",
            allowClear: true,
        });
        $(".js-example-basic-multiple10").select2({
            placeholder: "Select...",
            allowClear: true,
        });
    });

    new Vue({
        el:'#searchFilters',
        data:{
            exam_type:'',
            nationality:'',
            study_permit:'',
            edu_country:'',
            edu_level:'',
            grade_scheme:'',
            loader:false,
            programData:[],
            schoolData:[],
            countries:'',
            states:'',
            university:'',
            college:'',
            eng_institute:'',
            high_school:'',
            schools:'',
            tag1:'',
            tag2:'',
            programLength:'0',
            schoolLength:'0',
            range1:'',
            range2:'',
            program_level:'',
            country_name:'',
            school_name:'',
            state:'',
            fullLoader:false,
            program_type:''
        },
        methods:{
            enterClick: function (e){
                if (e.keyCode === 13) {
                    this.searchData();
                }
            },
            clearFilter: function (){
                this.fullLoader = true;
                $('#tags').val('');
                $('#tags2').val('');
                $('#states').val('');
                $('#countries').val('');
                $('#schools').val('');
                $('#exam_type').val('');
                $('#grade_scheme').val('');
                $('#edu_level').val('');
                $('#country').val('');
                $('#nationality').val('');
                $('#study_permit').val('');
                $('#rangerSlier1').html('');
                $('#rangerSlier2').html('');
                $('#programLevel').val('');
                this.programData = [''];
                this.schoolData = [''];
                this.schoolLength = '0';
                this.programLength = '0';
                let that = this;
                setTimeout(() => {
                    window.location.reload();
                },100);
            },
            searchData: function (){
                this.loader = true;
                this.tag1 = $('#search').val();
                this.tag2 = $('#search2').val();
                let url = '/search-results?header=true&country_name='+this.country_name+'&school_name='+this.school_name
                    +'&state='+this.state+'&tag1='+this.tag1+'&tag2='+this.tag2;
                this.programData = [''];
                this.schoolData = [''];
                this.$http.get(url).then((response) => {
                    this.programData = [''];
                    this.schoolData = [''];
                    if(response.data.school.length > 0 && response.data.program.length > 0) {
                        this.programLength = response.data.program.length;
                        this.schoolLength = response.data.school.length;
                        this.schoolData = response.data.school;
                        this.programData = response.data.program;
                        this.loader = false;
                    } else if(response.data.school.length > 0) {
                        response.data.school.map((response) => {
                            if (response) {
                                response.program.map((data) => {
                                    if (data) {
                                        this.programData.push(data);
                                    }
                                });
                            }
                        });

                        let arr = [];
                        this.programData.map((data) => {
                            if(data) {
                                arr.push(data);
                            }
                        });
                        this.programLength = arr.length;
                        this.programData = arr;
                        let that = this;
                        setTimeout(() => {
                            that.loader = false;
                        },1000);
                        that.schoolLength = response.data.school.length;
                        that.schoolData = response.data.school;
                    } else if(response.data.program.length > 0) {
                        response.data.program.map((response) => {
                            if(response) {
                                response.schools.map((data) => {
                                    if (data) {
                                        this.schoolData.push(data);
                                    }
                                });
                            }
                        });
                        let arr1 = [];
                        this.schoolData.map((data) => {
                            if(data) {
                                arr1.push(data);
                            }
                        });

                        this.schoolLength = arr1.length;
                        this.schoolData = arr1;

                        let that = this;
                        setTimeout(() => {
                            that.loader = false;
                        },1000);
                        that.programLength = response.data.program.length;
                        that.programData = response.data.program;
                    } else {
                        console.log('elexe case ssuccess');
                        this.schoolData = response.data.school;
                        this.programData = response.data.program;
                        this.loader = false;
                    }
                }).catch((error) => {
                    console.log('error');
                    console.log(error);
                });
            },
            getSchoolName: function (id) {
                console.log(id);
                let name = '';
                this.schoolData.map((data) => {
                   if(data.id == id) {
                       name = data.name;
                   }
                });
                let namee = ''
                if(name == '') {
                    let url = '/get/school/'+id;
                    this.$http.get(url).then((response) => {
                        namee = response.data.data.name;
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
                console.log('name namee ');
                console.log(name , namee);
                if(name) {
                    return name;
                } else {
                    return namee;
                }
            },
            getSchoolLogo: function (id) {
                console.log('school id');
                console.log(id);
                let logo_link = '';
                this.schoolData.map((data) => {
                    if(data.id == id) {
                        logo_link = data.logo_link;
                    }
                });
                let logo = '';
                if(logo_link == '') {
                    let url = '/get/school/'+id;
                    this.$http.get(url).then((response) => {
                        console.log(response.data.data.logo_link);
                        logo = response.data.data.logo_link;
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
                console.log('logo link ');
                console.log(logo_link , logo);
                if(logo_link != '') {
                    return logo_link;
                } else {
                    return logo;
                }
            },
            getSchoolCountry: function (id){
              console.log('school country');
              console.log(id);
              let country = '';
                this.schoolData.map((data) => {
                    if(data.id == id) {
                        country = data.country;
                    }
                });
                let  countr = '';
                if(country == '') {
                    let url = '/get/school/'+id;
                    this.$http.get(url).then((response) => {
                        console.log(response.data.data.country);
                        countr = response.data.data.country;
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
                    console.log('country ? country : countr');
                    console.log(country , countr);
                    if(country) {
                        return country;
                    } else {
                        return countr;
                    }
            },
            selectCheckbox: function (val){
                if(val == 'a') {
                   let university = $('#university').prop('checked');
                    this.university = university;
                }
                if(val == 'b') {
                    let college = $('#college').prop('checked');
                    this.college = college;
                }
                if(val == 'c') {
                    let eng_institute = $('#eng_institute').prop('checked');
                    this.eng_institute = eng_institute;
                }
                if(val == 'd') {
                    let high_school = $('#high_school').prop('checked');
                    this.high_school = high_school;
                }
            },
            applySchoolFilter: function (){
                this.loader = true;
                let states  = $('#states').val();
                let countries  = $('#countries').val();
                let schools  = $('#schools').val();
                this.countries = countries;
                this.schools = schools;
                this.states = states;
                let url = '/search-results?filter=true&countries='+this.countries+'&state='
                    +this.states+'&university='+this.university+'&college='+this.college
                    +'&eng_institute='+this.eng_institute+'&high_school='+this.high_school+'&schools='+this.schools;
                console.log('url');
                console.log(url);
                this.programData = [''];
                this.schoolData = [''];
                this.$http.get(url).then((response) => {
                    console.log('response');
                    console.log(response);
                    this.programData = [''];
                    this.schoolData = [''];
                    console.log(response.data.school);
                    response.data.school.map((response) => {
                        if(response) {
                            response.program.map((data) => {
                                if (data) {
                                    this.programData.push(data);
                                }
                            });
                        }
                    });
                    console.log('this.programData');
                    let arr = [];
                    this.programData.map((data) => {
                        if(data) {
                            arr.push(data);
                        }
                    });
                    this.programLength = arr.length;
                    this.programData = arr;
                    let that = this;
                    setTimeout(() => {
                        that.loader = false;
                    },1000);
                    console.log('this.programData');
                    console.log(that.programData);
                    that.schoolLength = response.data.school.length;
                    that.schoolData = response.data.school;
                }).catch((error) => {
                    console.log('error');
                    console.log(error);
                });
            },
            applyEligiblityFilter: function (){
                this.exam_type = $('#exam_type').val();
                this.grade_scheme = $('#grade_scheme').val();
                this.edu_level = $('#edu_level').val();
                this.edu_country = $('#country').val();
                this.nationality = $('#nationality').val();
                this.study_permit = $('#study_permit').val();
                console.log(this.nationality);
                console.log(this.edu_country);
                this.loader = true;
                this.programData = [''];
                this.schoolData = [''];
                let url = '/search-results?filter=true&nationality='+this.nationality+'&country='+this.edu_country+'&program_length='+this.edu_level;
                this.$http.get(url).then((response) => {
                    console.log('response');
                    console.log(response);
                    this.programData = [''];
                    this.schoolData = [''];
                    response.data.school.map((response) => {
                        if(response) {
                            response.program.map((data) => {
                                if (data) {
                                    this.programData.push(data);
                                }
                            });
                        }
                    });
                    let arr = [];
                    this.programData.map((data) => {
                        if(data) {
                            arr.push(data);
                        }
                    });
                    this.programLength = arr.length;
                    this.programData = arr;
                    let that = this;
                    setTimeout(() => {
                        that.loader = false;
                    },1000);
                    console.log('this.programData');
                    console.log(that.programData);
                    that.schoolLength = response.data.school.length;
                    that.schoolData = response.data.school;
                }).catch((error) => {
                    console.log('error');
                    console.log(error);
                });
            },
            applyProgramFilters: function (){
                this.loader = true;
                let rangeSlider1 = $('#rangerSlier1').html()
                let rangeSlider2 = $('#rangerSlier2').html()
                let programLevel = $('#programLevel').val();
                this.range1 = rangeSlider1;
                this.range2 = rangeSlider2;
                this.program_level = programLevel;
                let url;
                @if($filter == 'under_graduate')
                    this.program_type = 'under_graduate';
                    url = '/search-results?program_filters=true&program_type='+this.program_type;
                @elseif($filter == 'post_graduate')
                    this.program_type = 'post_graduate';
                    url = '/search-results?program_filters=true&program_type='+this.program_type;
                @else
                    url = '/search-results?program_filters=true&range1='+this.range1+'&range2='
                    +this.range2+'&program_level='+this.program_level;
                @endif
                console.log('url');
                console.log(url);
                this.programData = [''];
                this.schoolData = [''];
                this.$http.get(url).then((response) => {
                    console.log('response');
                    console.log(response);
                    this.programData = [''];
                    this.schoolData = [''];
                    console.log(response.data.program);
                    response.data.program.map((response) => {
                        if(response) {
                            response.schools.map((data) => {
                                if (data) {
                                    this.schoolData.push(data);
                                }
                            });
                        }
                    });
                    console.log('this.schoolData');
                    let arr = [];
                    this.schoolData.map((data) => {
                        if(data) {
                            arr.push(data);
                        }
                    });
                    this.programLength = response.data.program.length;
                    this.schoolLength = arr.length;
                    let that = this;
                    setTimeout(() => {
                        that.loader = false;
                    },1000);
                    console.log('this.programData');
                    that.programData = response.data.program;
                    console.log(that.programData);
                }).catch((error) => {
                    console.log('error');
                    console.log(error);
                });

            }
        },
        mounted(){
            console.log('search page initialized success');
            @if($filter == 'schools')
                this.applySchoolFilter();
            @endif
            @if($filter == 'under_graduate')
                this.applyProgramFilters();
            @elseif($filter == 'post_graduate')
                this.applyProgramFilters();
            @endif
        },
    });


    function selectCountry(event){
        console.log('event');
        console.log(event.target.value);
    }
</script>
<script src="{{asset('assets/js/select-chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/js/select-input.js')}}"></script>
<script>
    var route1 = "{{ url('/get/autocomplete/data') }}";
    var data = [];
    $.ajax({
        url:route1,
        type:'GET',

        success: function (response) {
            response.map((item) => {
                data.push({label:item.program_name,value:''})
            });

        },
        error: function (error) {
            console.log('error');
            console.log(error);
        }
    })
    $( "#search" ).autocomplete({
        source: function( request, response ) {
            var results = $.ui.autocomplete.filter(data, request.term);
            console.log('results.length');
            console.log(results.length);
            if (results.length <= 0) {
                var result = [
                    {
                        label: 'There are no matches for your query',
                        value: 'false'
                    }
                ];
                response(result);
                return;
            }
            var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
            response( $.grep( data, function( item ){
                return matcher.test( item.label );
            }) );
        },
        minLength: 1,
        select: function(event, ui) {
            if(ui.item.value != 'false') {
                event.preventDefault();
                $("#search").val(ui.item.label);
                $("#selected-tag").val(ui.item.label);
            }
            // window.location.href = ui.item.value;
        }
        ,
        focus: function(event, ui) {
            if(ui.item.value != 'false') {
                event.preventDefault();
                $("#search").val(ui.item.label);
            }
        }
    });

    var data1 = [];
    var route = "{{ url('/get/autocomplete/location/data') }}";
    $.ajax({
        url:route,
        type:'GET',

        success: function (response) {
            response.map((item) => {
                data1.push({label:item.name,value:''})
                data1.push({label:item.country,value:''})
            });

        },
        error: function (error) {
            console.log('error');
            console.log(error);
        }
    })
    var NoResultsLabel = "No Results Found";
    $( "#search2" ).autocomplete({
        source: function( request, response ) {
            var results = $.ui.autocomplete.filter(data1, request.term);
            if (results.length <= 0) {
                var result = [
                    {
                        label: 'There are no matches for your query',
                        value: 'false'
                    }
                ];
                response(result);
                return;
            }
            var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
            response( $.grep( data1, function( item ){
                return matcher.test( item.label );
            }) );
        },
        minLength: 1,
        select: function(event, ui) {
            if(ui.item.value != 'false') {
                event.preventDefault();
                $("#search2").val(ui.item.label);
                $("#selected-tag2").val(ui.item.label);
                // window.location.href = ui.item.value;
            }
        }
        ,
        focus: function(event, ui) {
            if(ui.item.value != 'false') {
                event.preventDefault();
                $("#search2").val(ui.item.label);
            }
        }
    });
</script>
<script type="text/javascript">

    $(document).ready(function(){

        //Multiple Select Chosen
        $(".multipleChosen").chosen({
            placeholder_text_multiple: "Select..." //placeholder
        });

        //Select2
        $(".multipleSelect2").select2({
            placeholder: "What's your rating" //placeholder
        });

    })

</script>
<script>
    $(function(){
        $('.select').jselect_search({
            fillable : true, // allow custom item on the dropdown upon search input trigger
            searchable:true, // set to searchable items
        });

        $('#state').attr('data-pagination',4);

        $('#state').jselect_search({
            fillable : true, // allow custom item on the dropdown upon search input trigger
            searchable : true, // set to searchable items
            on_top_edge : function(){
                if( parseInt( $('#state').attr('data-pagination') ) > 1 ){
                    $('#state').attr('data-pagination',parseInt( $('#state').attr('data-pagination') )-1);
                }
            },
            on_bottom_edge : function(){
                if( parseInt( $('#state').attr('data-pagination') ) >= 1 ){
                    $('#state').attr('data-pagination',parseInt( $('#state').attr('data-pagination') )+1);
                }
            }
        });
    });
</script>
<script>

    $(document).ready(function(){
        var rangeSlider = function(){
            var slider = $('.range-slider1'),
                range = $('.range-slider1 input[type="range"]'),
                value = $('.range-value');
            slider.each(function(){
                value.each(function(){
                    var value = $(this).prev().attr('value');
                    $(this).html(value);
                    $('#rangerSlier1').val(value)
                });
                range.on('input', function(){
                    $(this).next(value).html(this.value);
                    $('#rangerSlier1').val(value)
                });
            });
        };
        rangeSlider();
    });

    $(document).ready(function(){
        var rangeSlider2 = function(){
            var slider = $('.range-slider'),
                range = $('.range-slider input[type="range"]'),
                value = $('.range-value');
            slider.each(function(){
                value.each(function(){
                    var value = $(this).prev().attr('value');
                    $(this).html(value);
                    $('#rangerSlier2').val(value)
                });
                range.on('input', function(){
                    $(this).next(value).html(this.value);
                    $('#rangerSlier2').val(value)
                });
            });
        };
        rangeSlider2();
    });

</script>
@endpush
