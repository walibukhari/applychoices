@extends('layouts.default')

@push('css')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
        .panel-title {
            display: inline;
            font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }

        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        #hideBtn{
            background: #fff;
            color: #DAA520;
            border: 0px !important;
            padding: 10px 35px;
        }
        #hideBtn:hover{
            padding: 10px 35px;
            background: #DAA520;
            color: #fff;
            border: 0px !important;
        }
        #applicationForm{
            background: #fff;
            color: #DAA520;
            border: 0px !important;
            padding: 10px 35px;
        }
        #applicationForm:hover{
            padding: 10px 35px;
            background: #DAA520;
            color: #fff;
            border: 0px !important;
        }
    </style>
@endpush

@section('content')

<section class="Registerform-Banner">
    <img src="/assets/image/register-banner.png" width="1920" height="550" alt=""/>
</section>


{{--<section class="LoginBreadcrumb">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}

{{--            <div class="col-xl-12 col-sm-12 col-12">--}}
{{--                <ul>--}}
{{--                    <li><a href="#">Home</a></li>--}}
{{--                    <li><a href="#">Login</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
<div class="row" style="width: 100%;">
                @if(session()->has('success_msg'))
                    <div style="width:100%;margin-bottom:0px;" class="alert alert-success">
                        {{session()->get('success_msg')}}
                    </div>
                @endif
</div>
<div class="loading" id="loaderr" style="display: none;">Loading&#8230;</div>
<section class="Section-Registerform">
    <div class="container">
        <div class="row">

            <div class="col-xl-12 col-md-12 col-12">
                <div class="Registerform-Heading">
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s">Application Form</h1>
                </div>
            </div>
       <form action="{{route('application.stores',[$program->id])}}" id="applicationFormSubmit" method="POST">
           @csrf
           <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
            <div class="col-xl-12 col-md-12 col-12">
                <div class="row">

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.1s">
                            <label>Name</label>
                            <input type="text" name="name" id="name" placeholder="Enter Your Name" required="name">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.2s">
                            <label>Date Of Birth</label>
                            <input type="date" name="dob" id="dob" placeholder="Enter Data of Birth" required="dob">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.3s">
                            <label>Email</label>
                            <input type="text" name="email" id="email" placeholder="Enter Your Email" required="email">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.4s">
                            <label>Phone</label>
                            <input type="number" name="phone" id="phone" placeholder="Enter Your Phone Number" required="phone">
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.5s">
                            <label>Address</label>
                            <input type="text" name="address" id="address" placeholder="Enter Your Address" required="address">
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="RegisterMiddle-Head wow fadeInUp" data-wow-delay="0.6s">
                            <h2>Last Education</h2>
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.7s">
                            <label>Last Education</label>
                            <input type="text" name="last_edu" id="last_edu" placeholder="Enter Your Last Education" required="last_edu">
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.8s">
                            <label>Year Completion</label>
                            <input type="text" name="year_complete" id="year_complete" placeholder="Enter Last Year Education" required="year_complete">
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="RegisterMiddle-Head wow fadeInUp" data-wow-delay="0.9s">
                            <h2>English Language Proficiency</h2>
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">

                        <div class="appearedHead wow fadeInUp" data-wow-delay="0.10s">
                            <h6>Have you appeared in IELTS?</h6>
                        </div>

                        <div class="appearedHead wow fadeInUp" data-wow-delay="0.11s">
                            <label class="RadioBtn">Yes
                                <input type="radio" name="ielts" id="ielts">
                                <span class="Btncheckmark"></span>
                            </label>
                        </div>

                        <div class="appearedHead wow fadeInUp" data-wow-delay="0.12s">
                            <label class="RadioBtn">No
                                <input type="radio" name="ielts" id="ielts">
                                <span class="Btncheckmark"></span>
                            </label>
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="appearedHead wow fadeInUp" data-wow-delay="0.10s">
                            <h6>Are you planning to appear in future if yes.</h6>
                        </div>

                        <div class="appearedHead wow fadeInUp" data-wow-delay="0.11s">
                            <label class="RadioBtn">Yes
                                <input type="radio" name="future_plan" onchange="checkList('yes')" id="future_plan">
                                <span class="Btncheckmark"></span>
                            </label>
                        </div>

                        <div class="appearedHead wow fadeInUp" data-wow-delay="0.12s">
                            <label class="RadioBtn">No
                                <input type="radio" name="future_plan" onchange="checkList('no')" id="future_plan">
                                <span class="Btncheckmark"></span>
                            </label>
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="Register-form wow fadeInUp" data-wow-delay="0.14s">
                            <input type="text" name="what_results" id="what_results" placeholder="If Yes then What is the Result">
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="RegisterMiddle-Head wow fadeInUp" data-wow-delay="0.15s">
                            <h2>Employment</h2>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="row" id="appendHtml">
                        </div>

                        </div>
                    </div>
                </div>

            <div class="pull-right">
                <button type="button" class="btn btn-info" id="hideBtn" onclick="saveData()">Submit</button>
                <button type="button" class="btn btn-info"
                        id="applicationForm" style="display: none;"
                        onclick="submitForm()"
                >
                    <i style="display: none;" id="faSpinFS" class="fa fa-refresh fa-spin"></i>
                    Submit Application
                </button>
            </div>
        </form>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Payment Modal</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default credit-card-box">
                                        <div class="panel-heading display-table" >
                                            <div class="row display-tr" >
                                                <h3 class="panel-title display-td" >Payment Details</h3>
                                                <div class="display-td" >
                                                    <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            @if (Session::has('success'))
                                                <div class="alert alert-success text-center">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                    <p>{{ Session::get('success') }}</p>
                                                </div>
                                            @endif

                                            <form role="form"  class="require-validation"
                                                  data-cc-on-file="false"
                                                  data-stripe-publishable-key="pk_test_51HTlCJErqGk6bYCnDOVKMzjecf9LmotVZDM55h9s6q8ECXQZMWI88rnhGMQNtfEsjBSqYLcAK6DuPay48qN63yXz00NTDFtn8U"
                                                  id="payment-form"
                                            >
                                                @csrf
                                                <input type="hidden" name="stripe_key" id="stripe_key" value="pk_test_51HTlCJErqGk6bYCnDOVKMzjecf9LmotVZDM55h9s6q8ECXQZMWI88rnhGMQNtfEsjBSqYLcAK6DuPay48qN63yXz00NTDFtn8U" />
                                                <input type="hidden" name="program_id" id="program_id" value="{{$program->id}}" />
                                                <input type="hidden" name="user_id" id="user_id" value="{{\Auth::user()->id}}" />
                                                <input type="hidden" name="stripeToken" id="stripeToken" value="" />
                                                <div class='form-row row'>
                                                    <div class='col-md-12 form-group required'>
                                                        <label class='control-label'>Name on Card</label> <input
                                                            class='form-control' name="card_name" id = "name_of_card" size='4' type='text'>
                                                    </div>
                                                </div>

                                                <div class='form-row row'>
                                                    <div class='col-md-12 form-group card required'>
                                                        <label class='control-label'>Card Number</label> <input
                                                            autocomplete='off' name="card_number" class='form-control card-number' size='20'
                                                            type='text' id = "card_number">
                                                    </div>
                                                </div>

                                                <div class='form-row row'>
                                                    <div class='col-md-12 col-md-4 form-group cvc required'>
                                                        <label class='control-label'>CVC</label>
                                                        <input autocomplete='off'
                                                        class='form-control card-cvc' name="cvc" placeholder='ex. 311' size='4'
                                                        type='text' id="cvs_number">
                                                    </div>
                                                    <div class='col-md-12 col-md-4 form-group expiration required'>
                                                        <label class='control-label'>Expiration Month</label> <input
                                                            class='form-control card-expiry-month' id ="expiration_month" name="expire_date"  placeholder='MM' size='2'
                                                            type='text'>
                                                    </div>
                                                    <div class='col-md-12 col-md-4 form-group expiration required'>
                                                        <label class='control-label'>Expiration Year</label> <input
                                                            class='form-control card-expiry-year' onblur="this.value=removeSpaces(this.value);" name="expire_year" id="expiration_year" placeholder='YYYY' size='4'
                                                            type='text'>
                                                    </div>
                                                </div>

                                                <div class='form-row row'>
                                                    <div class='col-md-12 error form-group hide' style="display: none;">
                                                        <div class='alert-danger alert'>Please correct the errors and try
                                                            again.</div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary btn-lg btn-block" type="submit">
                                                            <i style="display: none;" id="faSpin" class="fa fa-refresh fa-spin"></i>
                                                            Pay Now ($100)
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        </div>
    </div>
</section>

@endsection

@push('js')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
    <script language="javascript" type="text/javascript">
        function removeSpaces(string) {
            return string.split(' ').join('');
        }
    </script>
    <script>
        function checkList(val){
            if(val == 'yes') {
                $('#what_results').show();
                $('#what_results').val('');
            }
            if(val == 'no') {
                $('#what_results').hide();
                $('#what_results').val(0);
            }
        }
        function submitForm(){
            $('#faSpinFS').show();
            $('#applicationFormSubmit').submit();
        }
        function submitPayment(token){
            $('#myModal').modal('hide');
            $('#loaderr').show();
            let stripe_key = $('#stripe_key').val();
            let program_id = $('#program_id').val();
            let user_id = $('#user_id').val();
            let name_of_card = $('#name_of_card').val();
            let card_number = $('#card_number').val();
            let cvs_number = $('#cvs_number').val();
            let expiration_month = $('#expiration_month').val();
            let expiration_year = $('#expiration_year').val();

            let form = new FormData();
            form.append('_token','{{csrf_token()}}');
            form.append('stripe_key',stripe_key);
            form.append('program_id',program_id);
            form.append('user_id',user_id);
            form.append('name_of_card',name_of_card);
            form.append('card_number',card_number);
            form.append('cvs_number',cvs_number);
            form.append('expiration_month',expiration_month);
            form.append('expiration_year',expiration_year);
            form.append('stripeToken',token);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'POST',
                url:'{{route('stripe.payment')}}',
                data:form,
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if(response.data.status == 'okay') {
                        $('#loaderr').hide();
                        $('#applicationForm').show();
                        $('#hideBtn').hide();
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            });
        }
        function saveData(){
            let modal = false;
            let name = $('#name').val();
            let dob = $('#dob').val();
            let email = $('#email').val();
            let phone = $('#phone').val();
            let address = $('#address').val();
            let year_complete = $('#year_complete').val();
            let last_edu = $('#last_edu').val();
            let ielts = $('#ielts').val();
            let future_plan = $('#future_plan').val();
            let what_results = $('#what_results').val();
            if(name == '') {
                toastr.error('please fill name');
            } else if(dob == '') {
                toastr.error('please fill date of birth');
            } else if(email == '') {
                toastr.error('please fill email');
            } else if(phone == '') {
                toastr.error('please fill phone');
            } else if(address == '') {
                toastr.error('please fill address');
            } else if(last_edu == '') {
                toastr.error('please fill last education');
            } else if(year_complete == '') {
                toastr.error('please fill last education');
            } else if(ielts == '') {
                toastr.error('please fill ielts section');
            } else if(ielts == '') {
                toastr.error('please fill ielts section');
            } else if(future_plan == '') {
                toastr.error('please fill future plan');
            } else {
                modal = true;
            }
            if(modal) {
                $('#myModal').modal({
                    show: true
                });
            }
        }



        var count = 0;
        var arr = [];
        function addFunction(e){
            e.stopPropagation();
            count++;
            let html = ' <div class="col-xs-11 col-md-11 col-11 arr-'+count+'">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-xl-3 col-md-3 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.16s">\n' +
                '                                            <input type="text" name="name_of_organization[]" id="name_of_organization1" placeholder="Name of Organization" required="name_of_organization">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.17s">\n' +
                '                                            <input type="number" name="form[]" id="form1" placeholder="From" required="form">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.18s">\n' +
                '                                            <input type="number" name="to[]" id="to" placeholder="TO" required="to">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.19s">\n' +
                '                                            <input type="text" name="designation[]" id="designation1" placeholder="Designation" required="designation">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.20s">\n' +
                '                                            <input type="number" name="salary[]" id="salary1" placeholder="Salary" required="salary">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-xl-1 col-md-1 col-12 arr1-'+count+'">\n' +
                '                                    <div class="Register-form wow fadeInUp" data-wow-delay="0.20s" style="text-align: center;">\n' +
                '                                        <button type="button" style="border: 0px;" onclick="deleteRow('+count+')"><i class="fa fa-minus"></i></button>\n' +
                '                                    </div>\n' +
                '                                </div>';
            $('#appendHtml').append(html);
        }

        function deleteRow(id){
            $(".arr-"+id).empty();
            $(".arr1-"+id).empty();
        }

        function addRow(){
            let html = ' <div class="col-xs-11 col-md-11 col-11 previous data">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-xl-3 col-md-3 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.16s">\n' +
                '                                            <input type="text" name="name_of_organization[]" id="name_of_organization" placeholder="Name of Organization" required="name_of_organization">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.17s">\n' +
                '                                            <input type="number" name="form[]" id="form" placeholder="From" required="form">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.18s">\n' +
                '                                            <input type="number" name="to[]" id="to" placeholder="TO" required="to">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.19s">\n' +
                '                                            <input type="text" name="designation[]" id="designation" placeholder="Designation" required="designation">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-xl-2 col-md-2 col-12">\n' +
                '                                        <div class="Register-form wow fadeInUp" data-wow-delay="0.20s">\n' +
                '                                            <input type="number" name="salary[]" id="salary" placeholder="Salary" required="salary">\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-xl-1 col-md-1 col-12">\n' +
                '                                    <div class="Register-form wow fadeInUp" data-wow-delay="0.20s" style="text-align: center;">\n' +
                '                                        <button type="button" style="border: 0px;" onclick="addFunction(event)" ><i class="fa fa-plus"></i></button>\n' +
                '                                        <button type="button" style="border: 0px; display: none;"><i class="fa fa-minus"></i></button>\n' +
                '                                    </div>\n' +
                '                                </div>';
            $('#appendHtml').html(html);
        }

        $(document).ready(function () {
            addRow();
        })

        function removeFunction(){
            alert(count);
        }
    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function() {
            var $form         = $(".require-validation");
            $('form.require-validation').bind('submit', function(e) {
                var $form         = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs       = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid         = true;
                $errorMessage.addClass('hide');
                $('.has-error').removeClass('has-error');
                $inputs.each(function(i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault();
                    }
                });

                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }

            });

            function stripeResponseHandler(status, response) {
                console.log('status, response');
                console.log(status, response);
                if (response.error) {
                    $('#loaderr').hide();
                    toastr.error(response.error.message);
                    $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    $('#faSpin').show();
                    // token contains id, last4, and card type
                    var token = response['id'];
                    $('#stripeToken').val(token);
                    // insert the token into the form so it gets submitted to the server
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    submitPayment(token);
                    // $form.get(0).submit();
                }
            }

        });
    </script>


@endpush
