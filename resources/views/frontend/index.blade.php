@extends('layouts.default')


@push('css')
    <style>
        .Section-BannerBg{
            background: url(/Banner.jpg) no-repeat center top;
            background-size: cover;
            margin: 0;
            padding: 130px 0;
            height:auto;
        }
    </style>
@endpush


@section('content')

    @if(isset($user) && $user->register_as == '')
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Please Select First You Register As</h5>
                    </div>
                    <form method="POST" id="registerAsForm" action="{{route('registerAs')}}">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="containerAlert" style="color:red;display:none;">
                                    </div>
                                    <label>Register As</label>
                                    <select name="register_as" id="register_as" class="form-control">
                                        <option selected disabled>select type</option>
                                        <option value="1">student</option>
                                        <option value="2">school partner</option>
                                        <option value="3">recruiter</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="checkTypeSelected()" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
    <section class="Section-BannerBg">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-12">
                    <div class="BannerHead">
                        <h1 class="wow fadeInUp" data-wow-delay="0.4s">We're on a Mission<br /> to Educate the <span>World</span></h1>
                        <p class="wow fadeInUp" data-wow-delay="0.5s">ApplyChoices connects international students<br /> and recruitment partners to educational<br /> opportunities at institutions around the world.</p>
                        <a class="wow fadeInUp" href="#" data-wow-delay="0.6s">How it Works</a>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-12">
                    <div class="MainThumable-Container">
                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-12">
                                <div class="BannerThumble-Content wow fadeInUp" data-wow-delay="0.4s">
                                    <h1>Students</h1>
                                    <p>Students are provided with the platform to coordinatee and apply to programs in multiple universities that line up with their experience, aptitudes, and interests.</p>
                                    <a href="{{route('studentPage')}}">Get Started</a>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12">
                                <div class="BannerThumble-Content wow fadeInUp" data-wow-delay="0.6s">
                                    <h1>Partner Schools</h1>
                                    <p>Expand your strength by captivating qualified students matching prescribed criteria from nations around the globe.</p>
                                    <a href="{{route('schoolPartner')}}">Get Started</a>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-12">
                                <div class="BannerThumble-Content wow fadeInUp" data-wow-delay="0.8s">
                                    <h1>Recruitment Partners</h1>
                                    <p>Providing students with the most prominent opportunity for progress with access to the top 1,200+ institutions.</p>
                                    <a href="{{route('agentPage')}}">Get Started</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <section class="Section-AboutUs">
        <div class="container">
            <div class="row">
                    <div class="AboutUs-Head wow fadeInUp" data-wow-delay="0.2s">
                        <h2>About Us</h2>
                        <p>
                            ApplyChoices is a global online meeting point for students and schools around the globe. Here, imminent students can rapidly and effectively find applicable data regarding their choice to study abroad , while schools and higher education foundations can market themselves successfully to the right audience. We make the process of finding a suitable university, the application process, and the recruitment process easy and simple to gather international students and academic institutions at one platform. We are committed to helping students to succeed in their careers and take customer’s feedback to help us innovate and improve.
                        </p>

                        <ul>
                            <li>We help everyone in the world to locate their future instruction program at the right institution.</li>
                            <li>We inspire and enable the young generation to investigate their interests and become leaders.</li>
                            <li>We are an information hub that brings prospective students and universities together for an ideal fit.</li>
                        </ul>

                    </div>
                </div>

                <div class="col-xl-4 col-md-4 col-12">
                    <div class="AboutUs-Image wow fadeInUp" data-wow-delay="0.3s">
                        <img src="{{asset('assets/image/about-us-image.png')}}" width="380" height="450" alt=""/>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="Section-StudentsAllow">
        <div class="container">
            <div class="row">

                <div class="col-xl-6 col-md-6 col-12 d-none d-sm-block">
                    <div class="AboutUs-Image wow fadeInUp" data-wow-delay="0.2s">
                        <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt=""/>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-12">
                    <div class="StudentsAllow-Head wow fadeInUp" data-wow-delay="0.3s">
                        <h3>STUDENTS</h3>
                        <h4>Pursue Your Dream <br />of Studying Abroad</h4>
                        <p>Utilizing the ApplyChoices Platform, students find their prescribed programs and apply them to learn at institutions that best meet their experience and criteria. ApplyChoices provides step by step guidance through its intelligent network eradicating every hurdle which students face from finding the best university, going through the application process to the final step of visa approval, and beginning their excursion abroad.</p>
                        <a href="{{route('studentPage')}}">Get Started Now</a>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-12 d-xl-none d-block d-sm-none">
                    <div class="AboutUs-Image wow fadeInUp" data-wow-delay="0.2s">
                        <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt=""/>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="Section-AboutUs m-0">
        <div class="container">
            <div class="row">

                <div class="col-xl-6 col-md-6 col-12">
                    <div class="StudentsAllow-Head wow fadeInUp" data-wow-delay="0.2s">
                        <h3>PARTNER SCHOOLS</h3>
                        <h4>Reach Diverse and<br /> Qualified Students</h4>
                        <p>Having worked with ApplyChoices, partner schools attract the bulk of potential students and recruit them through its vast network spread around the globe. Using ApplyChoices Portfolio the respective universities audit all applications of students before accommodation, guaranteeing that students are qualified and that applications align with the school's rules.</p>
                        <a href="{{route('schoolPartner')}}">Learn How</a>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-12">
                    <div class="AboutUs-Image wow fadeInUp" data-wow-delay="0.3s">
                        <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt=""/>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="Section-StudentsAllow">
        <div class="container">
            <div class="row">

                <div class="col-xl-6 col-md-6 col-12 d-none d-sm-block">
                    <div class="AboutUs-Image wow fadeInUp" data-wow-delay="0.2s">
                        <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt=""/>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-12">
                    <div class="StudentsAllow-Head wow fadeInUp" data-wow-delay="0.3s">
                        <h3>Agents</h3>
                        <h4>Pursue Your Dream <br />of Studying Abroad</h4>
                        <p>ApplyChoices has many agents and recruitment partners which allow bridge between students and universities to make the application process simple to understand for students. Recruitment partners around the world are seeking new choices to improve access to education through the ApplyChoices platform.</p>
                        <a href="{{route('agentPage')}}">Get Started Now</a>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6 col-12 d-xl-none d-block d-sm-none">
                    <div class="AboutUs-Image wow fadeInUp" data-wow-delay="0.2s">
                        <img src="{{asset('assets/image/students.png')}}" width="570" height="500" alt=""/>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="Section-LogoBranding">
        <div class="container">
            <div class="row">

                <div class="col-xl-12 col-md-12 col-12">
                    <div class="LogoBranding-Head wow fadeInUp" data-wow-delay="0.2s">
                        <h4>Access to More <span>Opportunities</span></h4>
                        <p>What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing<br /> and typesetting industry Lorem Ipsum has been standard</p>
                    </div>
                </div>

                <div class="col-xl-12 col-md-12 col-12">
                    <div id="Branding-Slider" class="owl-carousel wow fadeInUp" data-wow-delay="0.3s">

                        <div class="LogoBrand-Image">
                            <a href="#" target="_blank"><img src="{{asset('assets/image/branding-01.png')}}" width="155" height="75" alt=""/></a>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-02.png')}}" width="247" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-03.png')}}" width="312" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-04.png')}}" width="340" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-05.png')}}" width="257" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-06.png')}}" width="1147" height="300" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-01.png')}}" width="155" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-02.png')}}" width="247" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-03.png')}}" width="312" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-04.png')}}" width="340" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-05.png')}}" width="257" height="75" alt=""/>
                        </div>

                        <div class="LogoBrand-Image">
                            <img src="{{asset('assets/image/branding-06.png')}}" width="1147" height="300" alt=""/>
                        </div>

                    </div>
                </div>

                <div class="col-xl-12 col-md-12 col-12">
                    <div class="SliderBtn wow fadeInUp" data-wow-delay="0.3s">
                        <a href="{{route('searchPage')}}">Explore Institutions</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="Section-ApplyChoices">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-12">

                    <div class="ApplyChoices-Head wow fadeInUp" data-wow-delay="0.2s">
                        <h2>Get Started with ApplyChoices</h2>
                    </div>

                    <div class="applyList-Btn">
                        <ul>
                            <li class="wow fadeInUp" data-wow-delay="0.2s"><a href="{{route('studentPage')}}">Students</a></li>
                            <li class="wow fadeInUp" data-wow-delay="0.4s"><a href="{{route('schoolPartner')}}">Partner School</a></li>
                            <li class="wow fadeInUp" data-wow-delay="0.6s"><a href="{{route('agentPage')}}">Agents</a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection


@push('js')
    <script>
        @if(isset($user) && $user->register_as == '')
            $(document).ready(function(){
                $("#myModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        @endif

        function checkTypeSelected(){
            let value = $('#register_as').val();
            if (value == null) {
                $('#containerAlert').show();
                $('#containerAlert').html('Please Select Type First');
            } else {
                $('#registerAsForm').submit();
            }
        }
    </script>
@endpush
