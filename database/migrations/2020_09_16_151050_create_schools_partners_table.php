<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('schools_partners');
        Schema::create('schools_partners', function (Blueprint $table) {
            $table->id();
            $table->longText('school_name');
            $table->longText('contact_first_name');
            $table->longText('contact_last_name');
            $table->string('contact_email');
            $table->longText('phone_number');
            $table->longText('contact_title');
            $table->longText('class_partners');
            $table->longText('additional_partners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools_partners');
    }
}
