<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_programs', function (Blueprint $table) {
            $table->id();
            $table->longText('university')->nullable();
            $table->string('country')->nullable();
            $table->bigInteger('tuition_fee')->nullable();
            $table->bigInteger('application_fee')->nullable();
            $table->bigInteger('program_level')->nullable();
            $table->longText('program_length')->nullable();
            $table->longText('cost_of_living')->nullable();
            $table->longText('university_logo_link_src')->nullable();
            $table->longText('description')->nullable();
            $table->longText('program')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_programs');
    }
}
