<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|-------------------------------------------------------admin.add.sub_category-------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('check')->group(function (){
    Route::get('admin/login','AdminController@login')->name('admin.login');
    Route::post('post/login','AdminController@postLogin')->name('admin.post.login');
});


Route::prefix('admin')->middleware('admins')->group(function() {
    Route::get('/home','AdminController@index')->name('admin.home');
    Route::get('add-school','SchoolController@addSchool')->name('school.management');
    Route::get('all-school','SchoolController@allSchool')->name('all.school');
    Route::get('edit-school/{id}','SchoolController@editSchool')->name('edit.school');
    Route::get('delete-school/{id}','SchoolController@deleteSchool')->name('delete.school');
    Route::post('update-school','SchoolController@updateSchool')->name('update.school');
    Route::post('school-store','SchoolController@saveSchool')->name('store.school');

    Route::get('add-program','ProgramController@addProgram')->name('program.management');
    Route::get('edit-program/{id}','ProgramController@editProgram')->name('edit.program');
    Route::get('delete.program/{id}','ProgramController@deleteProgram')->name('delete.program');
    Route::post('program-store','ProgramController@saveProgram')->name('store.program');
    Route::post('update-program','ProgramController@updateProgram')->name('update.program');
    Route::get('all-program','ProgramController@allProgram')->name('all.program');
    //SchoolsPartnersRoutes
    Route::get('/school-partners','PartnerController@index')->name('school.partners');
    Route::get('/status-approves/{id}','PartnerController@approve')->name('status.approve');
    Route::get('/status-rejects/{id}','PartnerController@reject')->name('status.reject');
    //recruiters ROutes
    Route::get('/recruiters-index','RecruiterController@index')->name('recruiter.index');
    Route::get('/recruiters-approves/{id}','RecruiterController@approve')->name('recruiters.approve');
    Route::get('/recruiters-reject/{id}','RecruiterController@reject')->name('recruiters.reject');
    //chats routes

    Route::get('/chat-view','ChatController@index')->name('chat.views');

    Route::get('/get-recruiter','ChatController@get_all_recruiters')->name('get.recruiters');
    Route::post('/post-recruiter-message','ChatController@postMessage')->name('postMessage');
    Route::get('/post-get-message/{id}','ChatController@getMessage')->name('getMessage');

    //userchats routes

    Route::get('/user-chat-view','ChatController@adminChat')->name('adminChat');

    Route::get('/get-students','ChatController@getStudents')->name('getStudents');
    Route::get('/all-transaction','AdminController@getTransaction')->name('all.transaction');
    Route::get('/application-forms','AdminController@getApplications')->name('app.form');

    //import excel routes

    Route::get('/download-excel','ExcelController@index')->name('excelFile');
    //program excel routes
    
    Route::get('/program-excel','ExcelController@program')->name('programFile');
});

Route::get('/admin/logout','AdminController@logout')->name('admin.logout');
