<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('homePage');
});


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});

Auth::routes();

Route::get('stripe-payment', 'StripeController@handleGet');
Route::post('stripe-payment', 'StripeController@handlePost')->name('stripe.payment');

Route::get('/import_excel', 'ImportExcelController@index');
Route::post('/import_excel/import', 'ImportExcelController@import');

Route::get('/auth/redirect/{provider}','SocialController@redirect');
Route::get('/googlelogin/callback/{provider}','SocialController@callback');

Route::get('/redirect/facebook', 'SocialController@redirectFB');
Route::get('/callback/redirect','SocialController@callbackFB');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home','HomeController@homePage')->name('homePage');
Route::post('/user-register-as', 'HomeController@registerAs')->name('registerAs');

Route::middleware('checkRegisterAs')->group(function() {
    Route::get('/student','HomeController@studentPage')->name('studentPage');
    Route::get('/search','HomeController@searchPage')->name('searchPage');
    Route::get('/school-partner','HomeController@schoolPartner')->name('schoolPartner');
    Route::post('/school-partner/store','HomeController@store')->name('schools.store');
    Route::get('/recruiter','HomeController@agentPage')->name('agentPage');

    Route::middleware('auth')->group(function() {
        Route::get('/recruiter-dashboard', 'RecruiterController@agentDashboard')->name('agentDashboard');
        //chats routes
        Route::get('/user-chat', 'UserChatController@index')->name('user.chats');
        Route::get('/post-get-user-message/{id}/{name}', 'UserChatController@getMessages')->name('user.get.chats');
        Route::post('/post-message', 'UserChatController@postMessage')->name('postUserMessage');
        Route::post('/post-phonenumber', 'RecruiterController@phoneNumber')->name('phoneNumber');
        Route::get('/post-chat', 'RecruiterController@chatName')->name('chatName');

        Route::post('/post-name', 'RecruiterController@postName')->name('postName');

        Route::post('/post-upload-image', 'RecruiterController@postUploadImage')->name('postUploadImage');

        Route::get('/user-profile', 'HomeController@userProfile')->name('user.profile');
        Route::post('/update-user-profile', 'HomeController@updateProfile')->name('user.profile.update');

        Route::get('/recruiter-index', 'RecruiterController@index')->name('recruiter.index');
        Route::get('/recruiter-submit', 'RecruiterController@store')->name('recruiter.submit');

        Route::get('/application-form/{id}', 'HomeController@appForm')->name('appForm');
        Route::post('/application-store/{id}', 'HomeController@storeApplication')->name('application.stores');
        Route::get('/download/{id}', 'UserChatController@getDownload')->name('getDownload');
    });
});
Route::get('/logout','HomeController@recruiterLogout')->name('recruiterLogout');
Route::get('/user/logout','HomeController@userLogout')->name('userLogout');

Route::get('/search-results','HomeController@searchData')->name('searchData');
Route::get('/get/school/{id}','HomeController@schoolData')->name('schoolData');
Route::get('/get/autocomplete/data','HomeController@getAutoCompleteData')->name('getAutoCompleteData');
Route::get('/get/autocomplete/location/data','HomeController@locationData')->name('locationData');

//popup routes
Route::get('/render-view','PopController@index')->name('render.views');
Route::post('/render-store','PopController@store')->name('render.stores');

